package ro.tuc.dsrl.ds.handson.assig.three.queue.communication.content;

/**
 * Created by maria on 11/19/2016.
 */
public class StringContent implements MessageContent {

    private String content;

    public StringContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content;
    }
}
