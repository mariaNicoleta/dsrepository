package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.content.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.content.StringContent;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Starting point for the Producer Client application. This
 * application will send several messages to be inserted
 * in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
    private static final String HOST = "localhost";
    private static final int PORT = 8888;

    private ClientStart() {
    }

    public static void main(String[] args) {
        final QueueServerConnection queue = new QueueServerConnection(HOST, PORT);
        sendMessagesToQueue(queue);
        sendDvdsToQueue(queue);
    }

    private static void sendMessagesToQueue(QueueServerConnection queue) {
        try {
            for (int i = 0; i < 5; i++) {
                queue.writeMessage(new StringContent("this is email number " + i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendDvdsToQueue(QueueServerConnection queue) {
        List<DVD> DVDList = new LinkedList<>();
        DVDList.add(new DVD("Gone Girl", 2014, 256.36));
        DVDList.add(new DVD("Gone With the Wind", 1939, 356.36));
        DVDList.add(new DVD("Gone Baby Gone", 2007, 156.36));
        try {
            for (DVD dvd : DVDList) {
                queue.writeMessage(dvd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
