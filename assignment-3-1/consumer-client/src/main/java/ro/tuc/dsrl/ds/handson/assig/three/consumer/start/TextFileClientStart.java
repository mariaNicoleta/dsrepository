package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.content.MessageContent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 * Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Starting point for the Consumer Client application. This application
 * will run in an infinite loop and retrieve messages from the queue server
 * and send e-mails with them as they come.
 */
public class TextFileClientStart {

    private static final String MSG_PATH = "E:\\Computer Science\\DS\\DSRepository\\assignment-three\\consumer-client\\messages\\";
    private static final String MSG_TYPE = ".txt";

    private TextFileClientStart() {
    }

    public static void main(String[] args) {
        QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

        MessageContent message;
        BufferedWriter writer;

        while (true) {
            try {
                message = queue.readMessage();

                final Date currentTime = Calendar.getInstance().getTime();
                String timeLog = new SimpleDateFormat("ddHHmm").format(currentTime);

                int messageIndex = 0;
                String fileName = MSG_PATH + "Message " + messageIndex + MSG_TYPE;
                File messageFile = new File(fileName);
                while (messageFile.exists()) {
                    messageIndex++;
                    fileName = MSG_PATH + "Message " + messageIndex + MSG_TYPE;
                    messageFile = new File(fileName);
                }
                if(!messageFile.createNewFile()) {
                    System.out.println("File exists!");
                    return;
                }
                writer = new BufferedWriter(new FileWriter(messageFile));
                writer.write(message.toString());
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
