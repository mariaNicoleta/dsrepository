package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailHelper;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.content.MessageContent;

import java.io.IOException;

public class MailClientStart {

    private MailClientStart() {
    }

    public static void main(String[] args) {
        QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

        MailHelper mailHelper = new MailHelper("samSmithTestMail@gmail.com", "samsmithabc125");
        MessageContent message;

        while (true) {
            try {
                message = queue.readMessage();
                System.out.println("Sending mail " + message);
                mailHelper.sendMail("samSmithTestMail@gmail.com", "Message from queue", message.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
