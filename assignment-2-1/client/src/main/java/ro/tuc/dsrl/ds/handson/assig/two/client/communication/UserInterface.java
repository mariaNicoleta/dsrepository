package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class UserInterface {

    private JFrame frame;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    final JTextField yearTextField = new JTextField(12);
    final JTextField engineSizeTextField = new JTextField(12);
    final JTextField priceTextField = new JTextField(12);

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                new UserInterface();
                //window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     */
    public UserInterface() {
        initializeComponents();
        initializeEvents();
    }

    final JButton button = new JButton("Compute");

    private void initializeEvents() {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final String yearTextFieldText = yearTextField.getText();
                final String engineSizeTextFieldText = engineSizeTextField.getText();
                final String priceTextFieldText = priceTextField.getText();
                Car car = new Car(Integer.parseInt(yearTextFieldText),
                        Integer.parseInt(engineSizeTextFieldText),
                        Integer.parseInt(priceTextFieldText));
            }
        });
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initializeComponents() {
        /*frame = new JFrame();
        frame.setBounds(100, 100, 730, 489);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);*/
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(2, 2, 2, 2);
        //constraints.weighty = 1.0;                 // allows vertical dispersion
        addComponents(new JLabel("Year"), yearTextField, panel, constraints);
        addComponents(new JLabel("Engine size"), engineSizeTextField, panel, constraints);
        addComponents(new JLabel("Price"), priceTextField, panel, constraints);
        panel.add(button);
        addComponents(new JLabel("Tax"), new JTextField(12), panel, constraints);
        addComponents(new JLabel("Selling price"), new JTextField(12), panel, constraints);
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(panel);
        f.setSize(600, 400);
        f.setLocation(200, 200);
        f.setVisible(true);
    }

    private static void addComponents(JLabel label, JTextField textField, JPanel panel,
                                      GridBagConstraints constraints) {
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add(label, constraints);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(textField, constraints);
    }

}

