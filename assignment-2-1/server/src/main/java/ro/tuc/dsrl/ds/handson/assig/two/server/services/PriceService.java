package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of the price to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class PriceService implements IPriceService {

	/**
	 * Computes the selling price of a car
	 *
	 * @param c Car for which to compute the tax
	 * @return the selling price of a car
	 */
	public double computePrice(Car c) {
		final double purchasingPrice = c.getPurchasingPrice();
		final int year = c.getYear();
		if (purchasingPrice < 0) {
			throw new IllegalArgumentException("Purchasing price cannot be a negative value!");
		}
		return fancyPriceComputing(purchasingPrice, year);
	}

	/**
	 * Usually computational intensive calculus that
	 * requires more physical resources than are available on the client.
	 */
	private double fancyPriceComputing(double purchasingPrice, int year) {
		return purchasingPrice - (purchasingPrice / 7) * (2015 - year);
	}
}
