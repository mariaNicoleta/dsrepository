package com.flightproject.dto;

import com.flightproject.entity.State;
import com.flightproject.entity.UserProfile;

import java.util.HashSet;
import java.util.Set;

public class UserDTO {

    private int id;
    private String ssoId;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String state = State.ACTIVE.getState();
    private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

    public UserDTO() {
    }

    public UserDTO(int id, String ssoId, String password, String firstName, String lastName, String email, String state, Set<UserProfile> userProfiles) {
        this.id = id;
        this.ssoId = ssoId;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.state = state;
        this.userProfiles = userProfiles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    public static class Builder {
        private int nestedid;
        private String nestedssoId;
        private String nestedpassword;
        private String nestedfirstName;
        private String nestedlastName;
        private String nestedemail;
        private String nestedstate = State.ACTIVE.getState();
        private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder ssoId(String ssoId) {
            this.nestedssoId = ssoId;
            return this;
        }

        public Builder password(String password) {
            this.nestedpassword = password;
            return this;
        }

        public Builder firstname(String name) {
            this.nestedfirstName = name;
            return this;
        }

        public Builder lastname(String lastname) {
            this.nestedlastName = lastname;
            return this;
        }

        public Builder email(String email) {
            this.nestedemail = email;
            return this;
        }

        public Builder state(String nestedstate) {
            this.nestedstate = nestedstate;
            return this;
        }

        public Builder userProfiles(Set<UserProfile> userProfiles) {
            this.userProfiles = userProfiles;
            return this;
        }

        public UserDTO create() {
            return new UserDTO(nestedid, nestedssoId, nestedpassword, nestedfirstName, nestedlastName, nestedemail, nestedstate, userProfiles);
        }

    }
}
