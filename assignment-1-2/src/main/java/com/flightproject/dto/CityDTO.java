package com.flightproject.dto;

public class CityDTO {

    private Long id;
    private String name;
    private Double latitude;
    private Double longitude;

    public CityDTO() {
    }

    public CityDTO(Long id, String name, Double latitude, Double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return name +
                ", latitude=" + latitude +
                ", longitude=" + longitude;
    }

    public static class Builder {

        private Long nestedid;
        private String nestedname;
        private Double nestedaltitude;
        private Double nestedlongitude;

        public Builder id(Long id) {
            this.nestedid = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder latitude(Double altitude) {
            this.nestedaltitude = altitude;
            return this;
        }

        public Builder longitude(Double longitude) {
            this.nestedlongitude = longitude;
            return this;
        }

        public CityDTO create() {
            return new CityDTO(nestedid, nestedname, nestedaltitude, nestedlongitude);
        }
    }
}
