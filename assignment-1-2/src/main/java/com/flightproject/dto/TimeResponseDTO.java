package com.flightproject.dto;

import java.sql.Timestamp;
import java.util.Date;

public class TimeResponseDTO {

    private String version;
    private String location;
    private String offset;
    private String suffix;
    private String localtime;
    private String isotime;
    private String utctime;
    private String dst;

    public TimeResponseDTO() {}

    public TimeResponseDTO(String version, String location, String offset, String suffix, String localtime, String isotime, String utctime, String dst) {
        this.version = version;
        this.location = location;
        this.offset = offset;
        this.suffix = suffix;
        this.localtime = localtime;
        this.isotime = isotime;
        this.utctime = utctime;
        this.dst = dst;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getLocaltime() {
        return localtime;
    }

    public void setLocaltime(String localtime) {
        this.localtime = localtime;
    }

    public String getIsotime() {
        return isotime;
    }

    public void setIsotime(String isotime) {
        this.isotime = isotime;
    }

    public String getUtctime() {
        return utctime;
    }

    public void setUtctime(String utctime) {
        this.utctime = utctime;
    }

    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public boolean isOk() {
        return localtime != null;
    }

    /*private long dstOffset;
    private long  rawOffset;
    private String status;
    private String timeZoneId;
    private String timeZoneName;

    public TimeResponseDTO() {
    }

    public TimeResponseDTO(long dstOffset, long rawOffset, String status, String timeZoneId, String timeZoneName) {
        this.dstOffset = dstOffset;
        this.rawOffset = rawOffset;
        this.status = status;
        this.timeZoneId = timeZoneId;
        this.timeZoneName = timeZoneName;
    }

    public Date getLocalDate(long timeStamp) {
        // The local time of a given location is the sum of the timestamp parameter,
        // and the dstOffset and rawOffset fields from the result.
        final long sum = timeStamp + dstOffset + rawOffset;
        return new Date(sum * 1000);
    }

    public boolean isOk() {
        return "OK".equalsIgnoreCase(status);
    }

    public long getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(long dstOffset) {
        this.dstOffset = dstOffset;
    }

    public long getRawOffset() {
        return rawOffset;
    }

    public void setRawOffset(long rawOffset) {
        this.rawOffset = rawOffset;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }*/
}
