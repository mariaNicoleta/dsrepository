package com.flightproject.dto;

import java.util.Date;

public class FlightDTO {

    private Long id;
    private String type;
    private Long departure_city_id;
    private Long arrival_city_id;
    private Date departure_time;
    private Date arrival_time;

    public FlightDTO() {
    }

    public FlightDTO(Long id, String type, Long departure_city_id, Long arrival_city_id, Date departure_time, Date arrival_time) {
        this.id = id;
        this.type = type;
        this.departure_city_id = departure_city_id;
        this.arrival_city_id = arrival_city_id;
        this.departure_time = departure_time;
        this.arrival_time = arrival_time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getDeparture_city_id() {
        return departure_city_id;
    }

    public void setDeparture_city_id(Long departure_city_id) {
        this.departure_city_id = departure_city_id;
    }

    public Long getArrival_city_id() {
        return arrival_city_id;
    }

    public void setArrival_city_id(Long arrival_city_id) {
        this.arrival_city_id = arrival_city_id;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(Date departure_time) {
        this.departure_time = departure_time;
    }

    public Date getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(Date arrival_time) {
        this.arrival_time = arrival_time;
    }

    @Override
    public String toString() {
        return "Flight type " + type +
                ", departure time " + departure_time +
                ", arrival time " + arrival_time;
    }

    public static class Builder {

        private Long nestedid;
        private String nestedtype;
        private Long nesteddeparture_city_id;
        private Long nestedarrival_city_id;
        private Date nesteddeparture_time;
        private Date nestedarrival_time;

        public Builder id(Long id) {
            this.nestedid = id;
            return this;
        }

        public Builder type(String type) {
            this.nestedtype = type;
            return this;
        }

        public Builder departure_city_id (Long departure_city_id) {
            this.nesteddeparture_city_id = departure_city_id;
            return this;
        }

        public Builder arrival_city_id (Long arrival_city_id) {
            this.nestedarrival_city_id = arrival_city_id;
            return this;
        }

        public Builder departure_time(Date departure_time) {
            this.nesteddeparture_time = departure_time;
            return this;
        }

        public Builder arrival_time(Date arrival_time) {
            this.nestedarrival_time = arrival_time;
            return this;
        }

        public FlightDTO create() {
            return new FlightDTO(nestedid, nestedtype, nesteddeparture_city_id, nestedarrival_city_id, nesteddeparture_time, nestedarrival_time);
        }
    }
}
