package com.flightproject.controller;

import com.flightproject.dto.CityDTO;
import com.flightproject.dto.FlightDTO;
import com.flightproject.entity.FlightSearch;
import com.flightproject.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("/user/")
public class RegularUserController {

    @Autowired
    FlightService flightService;

    @RequestMapping(value = "/flights", method = RequestMethod.GET)
    public String viewFlights(ModelMap model) {
        FlightController.addAllFlightsToModel(flightService, model);
        return "user/viewFlight";
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public String findFlights(ModelMap model) {
        addFlightsToModel(model);
        return "user/findFlights";
    }

    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public String returnLocalFlightTime(ModelMap model, FlightSearch flightSearch) {
        final FlightDTO flightDTO = flightService.findById(flightSearch.getFlight());

        final CityDTO departureCity = flightService.findDepartureCity(flightDTO);
        final CityDTO arrivalCity = flightService.findArrivalCity(flightDTO);

        Long arrivalTimestamp = flightDTO.getArrival_time().getTime();
        Long departureTimestamp = flightDTO.getDeparture_time().getTime();

        final Date arrivalDate = TimeController.getDate(arrivalCity.getLatitude(), arrivalCity.getLongitude(), arrivalTimestamp);
        final Date departureDate = TimeController.getDate(departureCity.getLatitude(), departureCity.getLongitude(), departureTimestamp);
        if(arrivalDate == null || departureDate == null) {
            System.out.println("There are errors");
            return "user/findFlights";
        }
        addFlightsToModel(model);
        model.addAttribute("departureResponse", "Local departure time: " + departureDate);
        model.addAttribute("arrivalResponse", "Local arrival time: " + arrivalDate);
        return "user/findFlights";
    }

    private void addFlightsToModel(ModelMap model) {
        model.addAttribute("flights", flightService.findAll());
        model.addAttribute("flightSearch", new FlightSearch());
    }

}
