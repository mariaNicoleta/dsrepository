package com.flightproject.controller;

import com.flightproject.dto.CityDTO;
import com.flightproject.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("/admin/city")
public class CityController {

    @Autowired
    FlightService flightService;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String viewCities(ModelMap model) {
        List<CityDTO> cities = flightService.getCities();
        model.addAttribute("cities", cities);
        return "city/viewCities";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newCity(ModelMap model) {
        model.addAttribute("city", new CityDTO());
        return "city/newCity";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveCity(CityDTO city, ModelMap model) {
        flightService.save(city);
        model.addAttribute("success", "city" + city.getName() + " has been registered successfully!");
        return "city/citySuccess";
    }
}