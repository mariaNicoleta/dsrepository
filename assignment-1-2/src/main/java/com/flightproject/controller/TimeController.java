package com.flightproject.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class TimeController {

    private static final String API_KEY = "AIzaSyA7V1XrguiMXskgnL94gCOTlmrrGi9o4go";
    private static final RestTemplate restTemplate = new RestTemplate();

    protected static Date getDate(Double latitude, Double longitude, Long timestamp) {
        return getDate(latitude.toString(), longitude.toString(), timestamp / 1000);
    }

    private static Date getDate(String latitude, String longitude, Long timestamp) {
        final String timezoneUrl = getTimezoneUrl(latitude, longitude);
        String response = restTemplate.getForObject(timezoneUrl, String.class);
        return parseResponse(response);
        //final String timezoneUrl = getGoogleTimezoneUrl(latitude, longitude, timestamp);
        //TimeResponseDTO response = restTemplate.getForObject(timezoneUrl, TimeResponseDTO.class);
        //return response.isOk() ? (Date) response.getLocaltime() : null;
    }

    private static Date parseResponse(String response) {
        final String dateString = response.split("\n")[9].split(">")[1].split("<")[0];
        Date date = null;
        try {
            date = (new SimpleDateFormat("dd MMM yyyy HH:mm:ss")).parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private static String getTimezoneUrl(String latitude, String longitude) {
        return "http://new.earthtools.org/timezone-1.1/" + latitude + "/" + longitude;
    }

    private static String getGoogleTimezoneUrl(String latitude, String longitude, long timestamp) {
        return "https://maps.googleapis.com/maps/api/timezone/json?location="
                + latitude + ","
                + longitude + "&timestamp="
                + timestamp + "&key=" + API_KEY;
    }
}
