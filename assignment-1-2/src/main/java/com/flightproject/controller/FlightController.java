package com.flightproject.controller;

import com.flightproject.dto.CityDTO;
import com.flightproject.dto.FlightDTO;
import com.flightproject.errorhandler.FlightValidator;
import com.flightproject.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("/admin/flight")
public class FlightController {

    @Autowired
    FlightService flightService;

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String viewFlights(ModelMap model) {
        addAllFlightsToModel(flightService, model);
        return "flights/viewFlight";
    }

    /**
     * Needed for the view for the cities to be on the indexes as their ids.
     */
    private static CityDTO[] getCityArray(List<CityDTO> cities) {
        CityDTO[] cityArray = new CityDTO[50000];
        for (CityDTO city : cities) {
            cityArray[city.getId().intValue()] = city;
        }
        return cityArray;
    }

    @RequestMapping(value = "/{id}")
    public String viewFlight(ModelMap model, @PathVariable("id") long flightId) {
        FlightDTO flight = flightService.findById(flightId);
        model.addAttribute("flight", flight);
        List<CityDTO> cities = flightService.getCities();
        CityDTO[] cityArray = getCityArray(cities);
        model.addAttribute("cities", cities);
        model.addAttribute("departure", cityArray[flight.getDeparture_city_id().intValue()]);
        model.addAttribute("arrival", cityArray[flight.getArrival_city_id().intValue()]);
        return "flights/flightDetails";
    }

    @RequestMapping(value = "/updateFlight", method = RequestMethod.POST)
    public String updateFlight(@Valid FlightDTO flight, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            System.out.println("There are errors");
            return "flights/flightDetails";
        }
        flightService.update(flight);
        addAllFlightsToModel(flightService, model);
        return "flights/viewFlight";
    }

    @RequestMapping(value = "/{id}/delete")
    public String deleteFlight(@PathVariable("id") long flightId, ModelMap model) {
        flightService.delete(flightId);
        addAllFlightsToModel(flightService, model);
        return "flights/viewFlight";
    }

    static void addAllFlightsToModel(FlightService flightService, ModelMap model) {
        List<FlightDTO> flights = flightService.findAll();
        model.addAttribute("flights", flights);
        model.addAttribute("cities", getCityArray(flightService.getCities()));
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newFlight(ModelMap model) {
        model.addAttribute("flight", new FlightDTO());
        model.addAttribute("cities", flightService.getCities());
        return "flights/newFlight";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String saveFlight(@Valid FlightDTO flight, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            System.out.println("There are errors");
            model.addAttribute("flight", flight);
            return "flights/newFlight";
        }
        flightService.save(flight);
        model.addAttribute("success", "flight type " + flight.getType() + " has been registered successfully!");
        return "flights/newFlightSuccess";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        dateFormat.setLenient(true);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        webDataBinder.addValidators(new FlightValidator());
    }

}