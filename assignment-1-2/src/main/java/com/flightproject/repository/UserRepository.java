package com.flightproject.repository;

import com.flightproject.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findById(int id);

    User findBySsoId(String sso);

    List<User> findAll();

    void delete(User user);
}
