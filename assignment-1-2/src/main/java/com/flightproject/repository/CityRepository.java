package com.flightproject.repository;

import com.flightproject.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

	List<City> findAll();

	City findById(Long id);
}
