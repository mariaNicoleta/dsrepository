package com.flightproject.repository;

import com.flightproject.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FlightRepository extends JpaRepository<Flight, Long> {

	Flight findById(Long id);

	List<Flight> findAll();

	void delete(Flight flight);

}
