package com.flightproject.service;


import com.flightproject.dto.FlightDTO;
import com.flightproject.dto.CityDTO;
import com.flightproject.entity.City;
import com.flightproject.entity.Flight;
import com.flightproject.errorhandler.ResourceNotFoundException;
import com.flightproject.repository.CityRepository;
import com.flightproject.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("flightService")
@Transactional
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private CityRepository cityRepository;

    public void save(FlightDTO flight) {
        flightRepository.save(getFlightPOJO(flight));
    }

    private Flight getFlightPOJO(FlightDTO flight) {
        Flight flightPOJO = new Flight();
        flightPOJO.setId(flight.getId());
        flightPOJO.setType(flight.getType());
        flightPOJO.setArrival_city_id(flight.getArrival_city_id());
        flightPOJO.setArrival_time(flight.getArrival_time());
        flightPOJO.setDeparture_city_id(flight.getDeparture_city_id());
        flightPOJO.setDeparture_time(flight.getDeparture_time());
        return flightPOJO;
    }

    public void save(CityDTO city) {
        City cityPOJO = getCityPOJO(city);
        cityRepository.save(cityPOJO);
    }

    public CityDTO findArrivalCity(FlightDTO flightDTO) {
        final City arrivalCityPOJO = cityRepository.findById(flightDTO.getArrival_city_id());
        return getCityDTO(arrivalCityPOJO);
    }

    public CityDTO findDepartureCity(FlightDTO flightDTO) {
        final City departureCityPOJO = cityRepository.findById(flightDTO.getDeparture_city_id());
        return getCityDTO(departureCityPOJO);
    }

    private City getCityPOJO(CityDTO city) {
        City cityPOJO = new City();
        cityPOJO.setId(city.getId());
        cityPOJO.setName(city.getName());
        cityPOJO.setLatitude(city.getLatitude());
        cityPOJO.setLongitude(city.getLongitude());
        return cityPOJO;
    }

    public FlightDTO findById(Long id) {
        return getFlightDTO(flightRepository.findById(id));
    }

    private FlightDTO getFlightDTO(Flight flight) {
        if (flight == null) {
            throw new ResourceNotFoundException(Flight.class.getSimpleName());
        }
        return new FlightDTO.Builder()
                .id(flight.getId())
                .type(flight.getType())
                .departure_city_id(flight.getDeparture_city_id())
                .departure_time(flight.getDeparture_time())
                .arrival_city_id(flight.getArrival_city_id())
                .arrival_time(flight.getArrival_time())
                .create();
    }

    private CityDTO getCityDTO(City city) {
        if (city == null) {
            throw new ResourceNotFoundException(City.class.getSimpleName());
        }
        return new CityDTO.Builder()
                .id(city.getId())
                .name(city.getName())
                .longitude(city.getLongitude())
                .latitude(city.getLatitude())
                .create();
    }

    public List<FlightDTO> findAll() {
        List<Flight> flightPOJOList = flightRepository.findAll();
        List<FlightDTO> flightList = new ArrayList<>();
        for (Flight flightPOJO : flightPOJOList) {
            flightList.add(getFlightDTO(flightPOJO));
        }
        return flightList;
    }

    public void update(FlightDTO flight) {
        flightRepository.save(getFlightPOJO(flight));
    }

    public void delete(Long flight) {
        flightRepository.delete(flightRepository.findById(flight));
    }

    public List<CityDTO> getCities() {
        List<City> cityPOJOList = cityRepository.findAll();
        List<CityDTO> cityList = new ArrayList<>();
        for (City cityPOJO : cityPOJOList) {
            cityList.add(getCityDTO(cityPOJO));
        }
        return cityList;
    }

}
