package com.flightproject.service;

import com.flightproject.entity.UserProfile;
import com.flightproject.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userProfileService")
@Transactional
public class UserProfileService {
	
	@Autowired
	UserProfileRepository userProfileRepository;
	
	public List<UserProfile> findAll() {
		return userProfileRepository.findAll();
	}

	public UserProfile findByType(String type){
		return userProfileRepository.findByType(type);
	}

	public UserProfile findById(int id) {
		return userProfileRepository.findById(id);
	}
}
