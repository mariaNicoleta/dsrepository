package com.flightproject.service;

import com.flightproject.dto.UserDTO;
import com.flightproject.errorhandler.ResourceNotFoundException;
import com.flightproject.entity.User;
import com.flightproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserDTO findBySso(String sso) {
        return getUserDTO(userRepository.findBySsoId(sso));
    }

    private UserDTO getUserDTO(User user) {
        if (user == null) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }
        return new UserDTO.Builder()
                .id(user.getId())
                .ssoId(user.getSsoId())
                .password(user.getPassword())
                .firstname(user.getFirstName())
                .lastname(user.getFirstName())
                .email(user.getEmail())
                .state(user.getState())
                .userProfiles(user.getUserProfiles())
                .create();
    }

}
