package com.flightproject.errorhandler;

import com.flightproject.dto.FlightDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class FlightValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return FlightDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors error) {
        FlightDTO flight = (FlightDTO) o;

        if (flight.getType() == null || flight.getType().isEmpty()) {
            error.rejectValue("type", "flight.type", "Flight type must not be null!");
        }

        if (flight.getDeparture_city_id() == null) {
            error.rejectValue("departure_city_id", "flight.departure_city_id", "Incorrect departure city!");
        }

        if (flight.getArrival_city_id() == null) {
            error.rejectValue("arrival_city_id", "flight.arrival_city_id", "Incorrect arrival city!");
        }

    }
}
