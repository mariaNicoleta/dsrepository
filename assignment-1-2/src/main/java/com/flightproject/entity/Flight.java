package com.flightproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "flight")
public class Flight implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    private String type;

    @NotNull
    @Column(name = "departure_city_id", nullable = false)
    private Long departure_city_id;

    @NotNull
    @Column(name = "arrival_city_id", nullable = false)
    private Long arrival_city_id;

    @NotNull
    @Column(name = "departure_time", nullable = false)
    //@DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm")
    private Date departure_time;

    @NotNull
    @Column(name = "arrival_time", nullable = false)
    //@DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm")
    private Date arrival_time;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "city_flight",
            joinColumns = {@JoinColumn(name = "city_id")},
            inverseJoinColumns = {@JoinColumn(name = "flight_id")})
    private Set<City> cities = new HashSet<City>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getDeparture_city_id() {
        return departure_city_id;
    }

    public void setDeparture_city_id(Long departure_city_id) {
        this.departure_city_id = departure_city_id;
    }

    public Long getArrival_city_id() {
        return arrival_city_id;
    }

    public void setArrival_city_id(Long arrival_city_id) {
        this.arrival_city_id = arrival_city_id;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(Date departure_time) {
        this.departure_time = departure_time;
    }

    public Date getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(Date arrival_time) {
        this.arrival_time = arrival_time;
    }
}
