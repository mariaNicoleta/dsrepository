package com.flightproject.entity;

public class FlightSearch {
    private static final String FIELD_DELIMITER = ",";
    private static final String TIME_DELIMITER = "=";

    private long flight;

    public long getFlight() {
        return flight;
    }

    public void setFlight(long flight) {
        this.flight = flight;
    }

    /**
     * @param isDeparture true if departure time is required
     *                    false if arrival time is required
     * @return a string representing the time
     */
/*    public long getCityId(boolean isDeparture) {
        return Long.parseLong(flight.split(FIELD_DELIMITER)[isDeparture ? 2 : 3].split(TIME_DELIMITER)[1]);
    }*/
}
