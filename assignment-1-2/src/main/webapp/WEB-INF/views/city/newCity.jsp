<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>City Form</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<jsp:include page="../fragments/admin_header.jsp"/>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-container">

    <h1>New City Form</h1>

    <form:form method="POST" modelAttribute="city" cssClass="col-md-4 col-md-offset-2">

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="name">Name</label>
            <form:input type="text" path="name" id="name" class="form-control"/>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="latitude">Latitude</label>
            <form:input type="id" step="any" path="latitude" id="latitude" class="form-control"/>
            <div class="has-error">
                <form:errors path="latitude" class="help-inline"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="longitude">Longitude</label>
            <form:input type="id" step="any" path="longitude" id="longitude" class="form-control"/>
            <div class="has-error">
                <form:errors path="longitude" class="help-inline"/>
            </div>
        </div>
    </div>

    <div class="form-actions floatRight">
        <input type="submit" value="Save" class="btn btn-primary btn-sm"> or <a
            href="<c:url value='/admin' />">Cancel</a>
    </div>
</div>
</div>
</form:form>
</div>
</body>
</html>