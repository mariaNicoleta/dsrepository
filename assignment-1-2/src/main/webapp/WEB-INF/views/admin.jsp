<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Admin page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>
<body>
<jsp:include page="fragments/admin_header.jsp"/>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="success">
    Dear <strong>${user}</strong>, Welcome to the admin page!
    <br/>
    Would you like to <a href="<c:url value='/admin/flight/new' />">add some flights</a>?
    <br/>
</div>
</body>
</html>