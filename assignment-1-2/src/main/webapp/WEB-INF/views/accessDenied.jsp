<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>AccessDenied page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
<br/>
<br/>
<div class="container-fluid">
    <h1 class="error" style="color: red">Dear <strong>${user}</strong>, You are not authorized to access this page.</h1>
</div>
<br/>
<div class="container">
    <a class="btn btn-default" href="<c:url value="/home" />">Go home</a>
    <sec:authorize access="hasRole('USER') or hasRole('ADMIN')">
        OR <a href="<c:url value="/logout" />">Logout</a>
    </sec:authorize>
</div>

</body>
</html>