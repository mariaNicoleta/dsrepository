<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Flight Form</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<jsp:include page="../fragments/admin_header.jsp"/>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-container">

    <h1>New Flight Form</h1>

    <form:form method="POST" modelAttribute="flight" cssClass="col-md-4 col-md-offset-2">

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="type">Type</label>
            <form:input type="text" path="type" id="type" class="form-control"/>
                <%--<div class="has-error">
                    <form:errors path="type" class="help-inline"/>
                </div>--%>
            <form:errors path="type" cssClass="error" />
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="departure_city_id">Departure town</label>
            <form:select id="departure_city_id" path="departure_city_id" items="${cities}" var="city"
                         itemValue="id" class="form-control"/>
            <div class="has-error">
                <form:errors path="departure_city_id" class="help-inline"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="departure_time">Departure time</label>
            <form:input type="datetime-local" path="departure_time" id="departure_time" class="form-control"/>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="arrival_city_id">Arrival town</label>
            <form:select id="arrival_city_id" path="arrival_city_id" items="${cities}" var="city"
                         itemValue="id" class="form-control"/>
        </div>
        <div class="has-error">
            <form:errors path="arrival_city_id" class="help-inline"/>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="arrival_time">Arrival time</label>
            <form:input type="datetime-local" path="arrival_time" id="arrival_time" class="form-control"/>
        </div>
    </div>

    <div class="form-actions floatRight">
        <input type="submit" value="Save" class="btn btn-primary btn-sm"> or <a
            href="<c:url value='/admin' />">Cancel</a>
    </div>
</div>
</div>
</form:form>
</div>
</body>
</html>