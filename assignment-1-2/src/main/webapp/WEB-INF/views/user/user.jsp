<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Welcome page</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
<jsp:include page="../fragments/user_header.jsp"/>
<script src="/static/jquery-1.8.3.js"></script>
<script src="/static/bootstrap.js"></script>
<div class="success">
	${greeting}
	This is a welcome page. Enjoy!
</div>

</body>
</html>