<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="#">Flight management</a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
        </div>

        <div class="navbar-collapse collapse navbar-responsive-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<c:url value='/admin' />">Home</a></li>

                <li class="dropdown"><a href="#" class="dropdown-toggle"
                                        data-toggle="dropdown">Flights<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value='/admin/flight/new' />">Add</a></li>
                        <li><a href="<c:url value='/admin/flight/view' />">View</a></li>
                    </ul>
                </li>

                <li class="dropdown"><a href="#" class="dropdown-toggle"
                                        data-toggle="dropdown">Cities<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value='/admin/city/new' />">Add</a></li>
                        <li><a href="<c:url value='/admin/city/view' />">View</a></li>
                    </ul>
                </li>

                <li><a href="<c:url value="/logout" />">Logout</a></li>
            </ul>
        </div>
    </div>
</div>