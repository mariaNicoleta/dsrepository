package com.flightproject.tests;

import com.flightproject.dto.FlightDTO;
import com.flightproject.service.FlightService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class FlightServiceTest {

    @Autowired
    private FlightService flightService;

    @Test
    public void test() throws Exception {
        FlightDTO flightDTO = new FlightDTO.Builder()
                .id(50L)
                .type("dede")
                .departure_city_id(0L)
                .departure_time(new Date(84968))
                .arrival_city_id(1L)
                .arrival_time(new Date(59656))
                .create();

        flightService.save(flightDTO);

        List<FlightDTO> fromDB = flightService.findAll();

        assertTrue("One entity inserted", fromDB.size() == 1);
    }

}