USE `assignment-two-db`;

/*All User's gets stored in APP_USER table*/
CREATE TABLE APP_USER (
  id         BIGINT       NOT NULL AUTO_INCREMENT,
  sso_id     VARCHAR(30)  NOT NULL,
  password   VARCHAR(100) NOT NULL,
  first_name VARCHAR(30)  NOT NULL,
  last_name  VARCHAR(30)  NOT NULL,
  email      VARCHAR(30)  NOT NULL,
  state      VARCHAR(30)  NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (sso_id)
);
/* USER_PROFILE table contains all possible roles */
CREATE TABLE USER_PROFILE (
  id   BIGINT      NOT NULL AUTO_INCREMENT,
  type VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (type)
);

/* JOIN TABLE for MANY-TO-MANY relationship*/
CREATE TABLE APP_USER_USER_PROFILE (
  user_id         BIGINT NOT NULL,
  user_profile_id BIGINT NOT NULL,
  PRIMARY KEY (user_id, user_profile_id),
  CONSTRAINT FK_APP_USER FOREIGN KEY (user_id) REFERENCES APP_USER (id),
  CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES USER_PROFILE (id)
);

/* Populate USER_PROFILE Table */
INSERT INTO USER_PROFILE (type)
VALUES ('USER');

INSERT INTO USER_PROFILE (type)
VALUES ('ADMIN');

/* Populate one Admin User which will further create other users for the application using GUI */
INSERT INTO APP_USER (sso_id, password, first_name, last_name, email, state)
VALUES
  ('sam', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'Sam', 'Smith', 'samy@xyz.com', 'Active');
/* Populate JOIN Table */
INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
  SELECT
    user.id,
    profile.id
  FROM app_user user, user_profile profile
  WHERE user.sso_id = 'sam' AND profile.type = 'ADMIN';
INSERT INTO APP_USER (sso_id, password, first_name, last_name, email, state)
VALUES
  ('matt', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'Matt', 'Smith', 'mat@xyz.com', 'Active');
/* Populate JOIN Table */
INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
  SELECT
    user.id,
    profile.id
  FROM app_user user, user_profile profile
  WHERE user.sso_id = 'matt' AND profile.type = 'USER';

/**/
CREATE TABLE CITY (
  id        BIGINT NOT NULL AUTO_INCREMENT,
  latitude  INT    NOT NULL,
  longitude INT    NOT NULL,
  PRIMARY KEY (id)
);

/**/
CREATE TABLE FLIGHT (
  id            BIGINT      NOT NULL AUTO_INCREMENT,
  type              VARCHAR(30) NOT NULL,
  departure_city_id BIGINT      NOT NULL,
  arrival_city_id   BIGINT      NOT NULL,
  departure_time    DATETIME    NOT NULL,
  arrival_time      DATETIME    NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (`departure_city_id`)
  REFERENCES CITY (`id`)
    ON DELETE CASCADE,
  FOREIGN KEY (`arrival_city_id`)
  REFERENCES CITY (`id`)
    ON DELETE CASCADE
);

/* JOIN TABLE for MANY-TO-MANY relationship*/
CREATE TABLE CITY_FLIGHT (
  city_id   BIGINT NOT NULL,
  flight_id BIGINT NOT NULL,
  PRIMARY KEY (city_id, flight_id),
  CONSTRAINT FK_FLIGHT FOREIGN KEY (flight_id) REFERENCES FLIGHT (id),
  CONSTRAINT FK_CITY FOREIGN KEY (city_id) REFERENCES CITY (id)
);

ALTER TABLE `assignment-two-db`.city ADD name VARCHAR(30) NOT NULL;
ALTER TABLE `assignment-two-db`.city MODIFY latitude DOUBLE NOT NULL;
ALTER TABLE `assignment-two-db`.city MODIFY longitude DOUBLE NOT NULL;
