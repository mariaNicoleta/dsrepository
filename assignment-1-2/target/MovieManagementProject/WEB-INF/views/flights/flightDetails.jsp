<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Flight</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<jsp:include page="../fragments/admin_header.jsp"/>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-container">
    <h1>Flight details</h1>

    <div class="row">

        <form:form modelAttribute="flight" cssClass="col-md-4 col-md-offset-2" action="/flight-management/admin/flight/updateFlight" >

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="type">Type</label>
                    <div>
                        <form:input type="text" path="type" id="type" class="form-control" value="${flight.type}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="departure_city_id">Departure town</label>
                    <div>
                        <form:select id="departure_city_id" path="departure_city_id" items="${cities}" var="city"
                                     itemValue="id" class="form-control" value="departure"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="departure_time">Departure time</label>
                    <div>
                        <form:input type="datetime-local" path="departure_time" id="departure_time" class="form-control"
                                    value="${flight.departure_time}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="arrival_city_id">Arrival town</label>
                    <div>
                        <form:select id="arrival_city_id" path="arrival_city_id" items="${cities}" var="city"
                                     itemValue="id" class="form-control" value="arrival"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="arrival_time">Arrival time</label>
                    <div>
                        <form:input type="datetime-local" path="arrival_time" id="arrival_time" class="form-control"
                                    value="${flight.arrival_time}"/>
                    </div>
                </div>
            </div>


            <button type="submit" class="btn btn-default" href="<c:url value='/admin/flight/updateFlight' />" >Update</button>

        </form:form>

        <a class="btn btn-default"
           style="margin-left: 16px; margin-top: 10px;" type="submit"
           href='<c:url value="/admin/flight/${flight.id}/delete"/>'>Delete</a>

    </div>
</div>
</body>
</html>