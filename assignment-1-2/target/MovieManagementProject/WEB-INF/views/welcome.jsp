<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>
<body style="text-align: center">
<div class="success" style="text-align: center">
    ${greeting}

    <div style="padding: 10px 10px 10px 10px;">
        <a class="btn btn-default" href="<c:url value='/login' />">Log in</a>
    </div>

</div>
</body>
</html>