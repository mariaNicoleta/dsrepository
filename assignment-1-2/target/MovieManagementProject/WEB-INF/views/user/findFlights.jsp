<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Flight Form</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<jsp:include page="../fragments/user_header.jsp"/>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-container">

    <h1>Find flights</h1>

    <form:form method="POST" modelAttribute="flightSearch" cssClass="col-md-4 col-md-offset-2"
               action="/flight-management/user/find">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

    <div class="row">
        <div class="form-group">
            <label class="control-label" for="flight">Flight</label>
            <form:select id="flight" path="flight" itemValue="id" items="${flights}" class="form-control"/>
        </div>
    </div>

    <div class="form-actions floatRight">
        <input type="submit" value="Find" class="btn btn-primary btn-sm">
    </div>

    <div class="information">
        <p>
            <br>
            <c:out value="${departureResponse}"/>
            <br/>
            <br>
            <c:out value="${arrivalResponse}"/>
            <br/>
        </p>
    </div>

</div>
</div>
</form:form>
</div>

</body>
</html>