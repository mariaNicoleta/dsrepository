USE `assignment-two-two-db`;

CREATE TABLE APP_USER (
  id       BIGINT       NOT NULL AUTO_INCREMENT,
  password VARCHAR(100) NOT NULL,
  username VARCHAR(30)  NOT NULL,
  name     VARCHAR(30)  NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO APP_USER (username, name, password)
VALUES
  ('sam', 'Sam Smith', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm'),
  ('matt', 'Matt Smith', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm');

CREATE TABLE CAR (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  owner_id    BIGINT NOT NULL,
  year       INT    NOT NULL,
  engine_size INT    NOT NULL,
  price      DOUBLE NOT NULL,
  tax        DOUBLE,
  PRIMARY KEY (id),
  FOREIGN KEY (owner_id) REFERENCES APP_USER (id)
);

INSERT INTO CAR(owner_id, year, engine_size, price) VALUES
  (1, 2016, 200, 78900.25),
  (1, 1995, 105, 900),
  (2, 2000, 700, 5000);