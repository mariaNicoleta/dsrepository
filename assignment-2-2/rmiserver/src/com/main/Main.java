package com.main;

import com.rmi.PriceCalculator;
import com.rmi.TaxCalculator;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main {

    private void startServer(){
        try {
            // create on port 1097
            Registry registry = LocateRegistry.createRegistry(1097);

            // create a new service named myMessage
            registry.rebind("computePrice", new PriceCalculator());
            registry.rebind("computeTax", new TaxCalculator());

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("system is ready");
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.startServer();
    }
}