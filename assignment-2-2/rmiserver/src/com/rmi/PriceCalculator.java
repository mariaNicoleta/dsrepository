package com.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PriceCalculator extends UnicastRemoteObject implements Calculator {

    public PriceCalculator() throws RemoteException {

    }

    /**
     * Computes the selling price of a car
     */
    @Override
    public double compute(final double purchasingPrice, final int year, final double engineCapacity) throws RemoteException {
        if (purchasingPrice < 0) {
            throw new IllegalArgumentException("Purchasing price cannot be a negative value!");
        }
        return fancyPriceComputing(purchasingPrice, year);
    }

    /**
     * Usually computational intensive calculus that
     * requires more physical resources than are available on the client.
     */
    private double fancyPriceComputing(double purchasingPrice, int year) {
        return purchasingPrice - (purchasingPrice / 7) * (2015 - year);
    }
}
