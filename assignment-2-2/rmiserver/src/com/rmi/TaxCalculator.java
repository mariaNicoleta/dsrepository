package com.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class TaxCalculator extends UnicastRemoteObject implements Calculator {

    public TaxCalculator() throws RemoteException {

    }

    @Override
    public double compute(final double purchasingPrice, final int year, final double engineCapacity) throws RemoteException {
        if (engineCapacity <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if (engineCapacity > 1601) sum = 18;
        if (engineCapacity > 2001) sum = 72;
        if (engineCapacity > 2601) sum = 144;
        if (engineCapacity > 3001) sum = 290;
        return engineCapacity / 200.0 * sum;
    }
}
