<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>

<body>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-style-5">
    <form>
        <fieldset>
            <legend>Here are all your cars</legend>
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>Year</th>
                    <th>Engine</th>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>SellPx</th>
                </tr>
                </thead>

                <c:forEach var="car" items="${cars}">
                    <tr>
                        <td><span class="number">${car.id}</span></td>
                        <td>${car.year}</td>
                        <td>${car.engine_size}</td>
                        <td>${car.price}</td>
                        <td>${car.tax}</td>
                        <td>${car.selling_price}</td>
                    </tr>
                </c:forEach>
            </table>
        </fieldset>
    </form>
</div>

</body>
</html>