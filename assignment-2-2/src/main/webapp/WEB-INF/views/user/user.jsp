<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
</head>

<body>
<script src="<c:url value="/static/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/static/bootstrap.js"/>"></script>

<div class="form-style-5">
    <c:url var="computeUrl" value="/compute"/>
    <form:form modelAttribute="car" action="${computeUrl}">
        <fieldset>
            <legend><span class="number">1</span> Enter car details</legend>
            <input type="number" name="year" id="year" placeholder="Fabrication year">
            <input type="number" name="engine_size" id="engine_size" placeholder="Engine size">
            <input type="number" step="any" name="price" id="price" placeholder="Purchasing price">
        </fieldset>

        <fieldset>
            <legend><span class="number">2</span> Choose your action</legend>
            <input type="submit" value="Compute taxes" name="action"/>
            <input type="submit" value="Save car" name="action"/>
            <input type="submit" value="View cars" name="action"/>
        </fieldset>

        <fieldset>
            <legend><span class="number">3</span> Computed values</legend>
            <p><c:out value="${taxMessage}"/></p>
            <p><c:out value="${sellingPriceMessage}"/></p>
        </fieldset>
    </form:form>
</div>

</body>
</html>