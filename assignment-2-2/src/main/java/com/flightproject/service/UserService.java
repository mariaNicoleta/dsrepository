package com.flightproject.service;

import com.flightproject.dto.UserDTO;
import com.flightproject.entity.User;
import com.flightproject.errorhandler.ResourceNotFoundException;
import com.flightproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserDTO findByUsername(String username) {
        return getUserDTO(userRepository.findByUsername(username));
    }

    private UserDTO getUserDTO(User user) {
        if (user == null) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }
        return new UserDTO.Builder()
                .id(user.getId())
                .password(user.getPassword())
                .username(user.getPassword())
                .name(user.getName())
                .create();
    }

    public Long getIdOfUsername(String username) {
        UserDTO userDTO = findByUsername(username);
        return userDTO.getId();
    }
}
