package com.flightproject.service;


import com.flightproject.dto.CarDTO;
import com.flightproject.entity.Car;
import com.flightproject.errorhandler.ResourceNotFoundException;
import com.flightproject.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("carService")
@Transactional
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public void save(CarDTO car, double tax, double selling_price) {
        Car carPOJO = getCarPOJO(car, tax, selling_price);
        carRepository.save(carPOJO);
    }

    public List<CarDTO> findAll() {
        List<Car> carPOJOList = carRepository.findAll();
        List<CarDTO> carList = new ArrayList<>();
        for (Car carPOJO : carPOJOList) {
            carList.add(getCarDTO(carPOJO));
        }
        return carList;
    }

    private Car getCarPOJO(CarDTO car, double tax, double selling_price) {
        Car carPOJO = new Car();
        carPOJO.setId(car.getId());
        carPOJO.setOwner_id(car.getOwner_id());
        carPOJO.setYear(car.getYear());
        carPOJO.setEngine_size(car.getEngine_size());
        carPOJO.setPrice(car.getPrice());
        carPOJO.setTax(tax);
        carPOJO.setSelling_price(selling_price);
        return carPOJO;
    }

    private CarDTO getCarDTO(Car car) {
        if (car == null) {
            throw new ResourceNotFoundException(Car.class.getSimpleName());
        }
        return (new CarDTO.Builder())
                .id(car.getId())
                .owner_id(car.getOwner_id())
                .year(car.getYear())
                .engine_size(car.getEngine_size())
                .price(car.getPrice())
                .tax(car.getTax())
                .selling_price(car.getSelling_price())
                .create();
    }

}
