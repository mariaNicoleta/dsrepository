package com.flightproject.controller;

import com.flightproject.controller.rmi.ClientHandler;
import com.flightproject.dto.CarDTO;
import com.flightproject.service.CarService;
import com.flightproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    CarService carService;

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        if (isUser()) {
            model.addAttribute("user", getPrincipal());
            return "user/user";
        }
        model.addAttribute("greeting", "Hi, Welcome to the flight management site");
        return "welcomePage";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/compute", method = RequestMethod.POST)
    public String compute(@RequestParam String action, ModelMap model, @ModelAttribute CarDTO car) {
        switch (action) {
            case "Compute taxes":
                return handleComputeTaxes(model, car, false);
            case "Save car":
                car.setOwner_id(userService.getIdOfUsername(getPrincipal()));
                handleComputeTaxes(model, car, true);
            case "View cars":
                model.addAttribute("cars", carService.findAll());
                return "user/view";
            default:
                return "/user/user";
        }
    }

    @RequestMapping(value = "/computeTable", method = RequestMethod.POST)
    public String compute(@RequestParam String action, ModelMap model) {
        return "user/view";
    }

    private String handleComputeTaxes(ModelMap model, CarDTO car, boolean shouldSave) {
        final double purchasingPrice = car.getPrice();
        final int year = car.getYear().intValue();
        final double engineCapacity = car.getEngine_size();

        final double tax = ClientHandler.computeTax(purchasingPrice, year, engineCapacity);
        final double sellingPrice = ClientHandler.computePrice(purchasingPrice, year, engineCapacity);
        if(shouldSave) {
            carService.save(car, tax, sellingPrice);
        }

        model.addAttribute("taxMessage", getTaxMessage(tax));
        model.addAttribute("sellingPriceMessage", getSellingPriceMessage(sellingPrice));
        return "user/user";
    }

    private String getTaxMessage(double engine_size) {
        return "The tax for the car is: " + engine_size;
    }

    private String getSellingPriceMessage(Double price) {
        return "The selling price for the car is: " + price;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    private String getPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    private boolean isUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final SimpleGrantedAuthority userRole = new SimpleGrantedAuthority("ROLE_USER");
        return principal instanceof UserDetails && ((UserDetails) principal).getAuthorities().contains(userRole);
    }
}