package com.flightproject.controller.rmi;

import com.rmi.Calculator;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientHandler {

    public static double computePrice(final double purchasingPrice, final int year, final double engineCapacity) {
        try {
            Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1097);
            Calculator priceCalculator = (Calculator) myRegistry.lookup("computePrice");
            return priceCalculator.compute(purchasingPrice, year, engineCapacity);
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }

    public static double computeTax(final double purchasingPrice, final int year, final double engineCapacity) {
        try {
            Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1097);
            Calculator taxCalculator = (Calculator) myRegistry.lookup("computeTax");
            return taxCalculator.compute(purchasingPrice, year, engineCapacity);
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }
}
