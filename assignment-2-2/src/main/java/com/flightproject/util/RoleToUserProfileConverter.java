package com.flightproject.util;

import com.flightproject.entity.UserProfileType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleToUserProfileConverter implements Converter<Object, UserProfileType> {

    public UserProfileType convert(Object element) {
        return UserProfileType.USER;
    }
}