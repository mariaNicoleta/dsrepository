package com.flightproject.dto;

public class UserDTO {

    private Long id;
    private String password;
    private String username;
    private String name;

    public UserDTO() {
    }

    public UserDTO(Long id, String password, String username, String name) {
        this.id = id;
        this.password = password;
        this.username = username;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Builder {

        private Long nestedid;
        private String nestedpassword;
        private String nestedusername;
        private String nestedname;

        public Builder id(Long id) {
            this.nestedid = id;
            return this;
        }

        public Builder name(String name) {
            this.nestedname = name;
            return this;
        }

        public Builder username(String altitude) {
            this.nestedusername = altitude;
            return this;
        }

        public Builder password(String password) {
            this.nestedpassword = password;
            return this;
        }

        public UserDTO create() {
            return new UserDTO(nestedid, nestedpassword, nestedusername, nestedname);
        }
    }
}
