package com.flightproject.dto;

import java.text.DecimalFormat;

public class CarDTO {

    private Long id;
    private Long owner_id;
    private Long year;
    private Long engine_size;
    private Double price;
    private Double tax;
    private Double selling_price;

    public CarDTO() {
    }

    public CarDTO(Long id, Long owner_id, Long year, Long engine_size, Double price, Double tax, Double selling_price) {
        this.id = id;
        this.owner_id = owner_id;
        this.year = year;
        this.engine_size = engine_size;
        this.price = price;
        this.tax = tax;
        this.selling_price = selling_price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(Long owner_id) {
        this.owner_id = owner_id;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Long getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(Long engine_size) {
        this.engine_size = engine_size;
    }

    public Double getPrice() {
        return getFormattedValue(price);
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return getFormattedValue(tax);
    }

    private Double getFormattedValue(Double value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        if(value == null) {
            return null;
        }
        return Double.valueOf(decimalFormat.format(value));
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getSelling_price() {
        return getFormattedValue(selling_price);
    }

    public void setSelling_price(Double selling_price) {
        this.selling_price = selling_price;
    }

    public static class Builder {

        private Long nestedid;
        private Long nestedowner_id;
        private Long nestedyear;
        private Long nestedengine_size;
        private Double nestedprice;
        private Double nestedtax;
        private Double nestedselling_price;

        public Builder id(Long id) {
            this.nestedid = id;
            return this;
        }

        public Builder owner_id(Long owner_id) {
            this.nestedowner_id = owner_id;
            return this;
        }

        public Builder year(Long year) {
            this.nestedyear = year;
            return this;
        }

        public Builder engine_size(Long engine_size) {
            this.nestedengine_size = engine_size;
            return this;
        }

        public Builder price(Double price) {
            this.nestedprice = price;
            return this;
        }

        public Builder tax(Double tax) {
            this.nestedtax = tax;
            return this;
        }

        public Builder selling_price(Double selling_price) {
            this.nestedselling_price = selling_price;
            return this;
        }

        public CarDTO create() {
            return new CarDTO(nestedid, nestedowner_id, nestedyear, nestedengine_size, nestedprice, nestedtax, nestedselling_price);
        }
    }
}
