package com.flightproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "CAR")
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "owner_id", nullable = false)
    private Long owner_id;

    @NotNull
    @Column(name = "year", nullable = false)
    private Long year;

    @NotNull
    @Column(name = "engine_size", nullable = false)
    private Long engine_size;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "tax")
    private Double tax;

    @Column(name = "selling_price")
    private Double selling_price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(Long owner_id) {
        this.owner_id = owner_id;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Long getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(Long engine_size) {
        this.engine_size = engine_size;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(Double selling_price) {
        this.selling_price = selling_price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (id != null ? !id.equals(car.id) : car.id != null) return false;
        if (owner_id != null ? !owner_id.equals(car.owner_id) : car.owner_id != null) return false;
        if (year != null ? !year.equals(car.year) : car.year != null) return false;
        if (engine_size != null ? !engine_size.equals(car.engine_size) : car.engine_size != null) return false;
        if (price != null ? !price.equals(car.price) : car.price != null) return false;
        return tax != null ? tax.equals(car.tax) : car.tax == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (owner_id != null ? owner_id.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (engine_size != null ? engine_size.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (tax != null ? tax.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", owner_id=" + owner_id +
                ", year=" + year +
                ", engine_size=" + engine_size +
                ", price=" + price +
                ", tax=" + tax +
                '}';
    }
}
