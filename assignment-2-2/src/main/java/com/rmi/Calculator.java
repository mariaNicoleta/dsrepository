package com.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculator extends Remote {
    double compute(final double purchasingPrice, final int year, final double engineCapacity) throws RemoteException;
}