<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Login page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="
<c:url value="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css"/>"/>
</head>

<body>
<div class="form-style-5">
    <c:url var="loginUrl" value="/login"/>
    <form action="${loginUrl}" method="post">
        <c:if test="${param.error != null}">
            <div class="alert alert-danger">
                <p>Invalid username and password.</p>
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <div class="alert alert-success">
                <p>You have been logged out successfully.</p>
            </div>
        </c:if>
        <fieldset>
            <legend><span class="number">1</span> Please enter your credentials:</legend>
            <input type="text" class="form-control" id="username" name="username" placeholder="Your username" required>
            <input type="password" class="form-control" id="password" name="password" placeholder="Your password" required>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </fieldset>

        <input type="submit" value="Log in">

        <div class="error">
            <p><c:out value="${errorMessage}"/></p>
        </div>
    </form>
</div>

</body>
</html>