<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Login page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="
<c:url value="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css"/>"/>
</head>

<body>
<div class="form-style-5">
    <form>
        <fieldset>
            <legend>Welcome to the car tax price calculator web app!</legend>
            <a class="btn btn-default" href="<c:url value='/login' />">Log in</a>
        </fieldset>
    </form>
</div>

</body>
</html>