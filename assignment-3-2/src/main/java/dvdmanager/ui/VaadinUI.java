package dvdmanager.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import dvdmanager.entities.Customer;
import dvdmanager.entities.DVDEntity;
import dvdmanager.repository.CustomerRepository;
import dvdmanager.repository.DVDRepository;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("valo")
public class VaadinUI extends UI {

    private final CustomerRepository customerRepository;
    private final CustomerEditor customerEditor;

    private final DVDRepository dvdRepository;
    private final DVDEditor dvdEditor;

    private final Grid customerGrid;
    private final Grid dvdGrid;

    private final Button addNewCustomerBtn;
    private final Button addNewDVDBtn;

    @Autowired
    public VaadinUI(CustomerRepository customerRepository, CustomerEditor customerEditor,
                    DVDRepository dvdRepository, DVDEditor dvdEditor) {
        this.customerRepository = customerRepository;
        this.customerEditor = customerEditor;
        this.dvdEditor = dvdEditor;
        this.dvdRepository = dvdRepository;
        this.customerGrid = new Grid();
        this.dvdGrid = new Grid();
        this.addNewCustomerBtn = new Button("New customer", FontAwesome.PLUS);
        this.addNewDVDBtn = new Button("New DVD", FontAwesome.PLUS);
    }

    @Override
    protected void init(VaadinRequest request) {
        // build layout
        HorizontalLayout customerActions = new HorizontalLayout(addNewCustomerBtn);
        VerticalLayout customerMainLayout = new VerticalLayout(customerActions, customerGrid, customerEditor);
        customerActions.setSpacing(true);
        customerMainLayout.setMargin(true);
        customerMainLayout.setSpacing(true);

        HorizontalLayout dvdActions = new HorizontalLayout(addNewDVDBtn);
        VerticalLayout dvdMainLayout = new VerticalLayout(dvdActions, dvdGrid, dvdEditor);
        dvdActions.setSpacing(true);
        dvdMainLayout.setMargin(true);
        dvdMainLayout.setSpacing(true);

        setContent(new HorizontalLayout(dvdMainLayout, customerMainLayout));

        // Configure layouts and components
        customerGrid.setHeight(300, Unit.PIXELS);
        customerGrid.setColumns("id", "name", "email");

        dvdGrid.setHeight(300, Unit.PIXELS);
        dvdGrid.setColumns("id", "title", "year", "price");

        initActions();

        // Initialize listing
        listCustomers();
        listDvds();
    }

    private void initActions() {
        initCustomerActions();
        initDvdActions();
    }

    private void initCustomerActions() {
        customerGrid.addSelectionListener(e -> {
            if (e.getSelected().isEmpty()) {
                customerEditor.setVisible(false);
            } else {
                customerEditor.editCustomer((Customer) customerGrid.getSelectedRow());
            }
        });

        // Instantiate and edit new Customer the new button is clicked
        addNewCustomerBtn.addClickListener(e -> customerEditor.editCustomer(new Customer("", "")));

        // Listen changes made by the customerEditor, refresh data from backend
        customerEditor.setChangeHandler(() -> {
            customerEditor.setVisible(false);
            listCustomers();
        });
    }

    private void initDvdActions() {
        dvdGrid.addSelectionListener(e -> {
            if (e.getSelected().isEmpty()) {
                dvdEditor.setVisible(false);
            } else {
                dvdEditor.editDVD((DVDEntity) dvdGrid.getSelectedRow());
            }
        });

        // Instantiate and edit new Dvd the new button is clicked
        addNewDVDBtn.addClickListener(e -> dvdEditor.editDVD(new DVDEntity("", 0, 0)));

        // Listen changes made by the dvdEditor, refresh data from backend
        dvdEditor.setChangeHandler(() -> {
            dvdEditor.setVisible(false);
            listDvds();
        });
    }

    @SuppressWarnings("unchecked")
    private void listCustomers() {
        customerGrid.setContainerDataSource(new BeanItemContainer(Customer.class, customerRepository.findAll()));
    }

    @SuppressWarnings("unchecked")
    private void listDvds() {
        dvdGrid.setContainerDataSource(new BeanItemContainer(DVDEntity.class, dvdRepository.findAll()));
    }
}


