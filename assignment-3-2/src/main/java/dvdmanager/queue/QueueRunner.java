package dvdmanager.queue;

import dvdmanager.Application;
import dvdmanager.entities.DVDEntity;
import dvdmanager.message.DVDContent;
import dvdmanager.message.MessageContent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class QueueRunner {//implements CommandLineRunner {

    private static RabbitTemplate rabbitTemplate;
    private final ConfigurableApplicationContext context;

    public QueueRunner(RabbitTemplate rabbitTemplate, ConfigurableApplicationContext context) {
        QueueRunner.rabbitTemplate = rabbitTemplate;
        this.context = context;
    }

    /*@Override
    public void run(String... args) throws Exception {
        System.out.println("Sending message...");
        MessageContent message = new DVD("Gone Girl", 2014, 256.36);
        rabbitTemplate.convertAndSend(Application.queueName, message);
    }*/

    public static void sendMessage(DVDEntity dvd) {
        MessageContent messageContent = new DVDContent(dvd.getTitle(), dvd.getYear(), dvd.getPrice());
        rabbitTemplate.convertAndSend(Application.queueName, messageContent);
    }
}
