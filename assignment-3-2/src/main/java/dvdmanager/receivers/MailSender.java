package dvdmanager.receivers;

import dvdmanager.entities.Customer;
import dvdmanager.message.MessageContent;
import dvdmanager.repository.CustomerRepository;
import dvdmanager.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toCollection;

@Component
public class MailSender {

    private static MailService mailService = new MailService("samSmithTestMail@gmail.com", "samsmithabc125");

    private static CustomerRepository customerRepository;

    @Autowired
    public MailSender(CustomerRepository repository) {
        customerRepository = repository;
    }

    protected static void notifySubscribers(MessageContent message) {
        List<String> subscribers = getAllEmails();
        for (String email : subscribers) {
            mailService.sendMail(email, "Message from queue", message.toString());
        }
    }

    private static List<String> getAllEmails() {
        final List<Customer> allCustomers = customerRepository.findAll();
        return allCustomers.stream().map(Customer::getEmail).collect(toCollection(LinkedList::new));
    }
}
