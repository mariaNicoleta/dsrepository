package dvdmanager.receivers;

import dvdmanager.message.MessageContent;

public class Receiver {

    private static void receiveMessage(MessageContent message) {
        System.out.println("Received <" + message.toString() + ">");
        MailSender.notifySubscribers(message);
        TextFileHelper.log(message);
    }
}
