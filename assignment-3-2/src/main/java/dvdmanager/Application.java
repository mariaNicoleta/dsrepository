
package dvdmanager;

import dvdmanager.entities.Customer;
import dvdmanager.entities.DVDEntity;
import dvdmanager.receivers.Receiver;
import dvdmanager.repository.CustomerRepository;
import dvdmanager.repository.DVDRepository;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public final static String queueName = "spring-boot";

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("spring-boot-exchange");
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(queueName);
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    Receiver receiver() {
        return new Receiver();
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

    @Bean
    public CommandLineRunner loadData(CustomerRepository customerRepository, DVDRepository dvdRepository) {
        return (args) -> {
            // save a couple of customers
            customerRepository.save(new Customer("Sam Smith", "samSmithTestMail@gmail.com"));
            customerRepository.save(new Customer("Jane Doe", "maria.pandrea13@gmail.com"));

            // save some existing DVD's
            dvdRepository.save(new DVDEntity("Gone Girl", 2014, 256.36));
            dvdRepository.save(new DVDEntity("Gone With the Wind", 1939, 356.36));
            dvdRepository.save(new DVDEntity("Gone Baby Gone", 2007, 156.36));
        };
    }

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
    }

}
