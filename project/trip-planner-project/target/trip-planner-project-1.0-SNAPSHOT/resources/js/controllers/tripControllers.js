(function () {

    var tripsModule = angular.module('tripControllers', ['ngRoute', 'ngMap'])

    tripsModule.constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        user: 'user'
    });

    tripsModule.config(function ($routeProvider, USER_ROLES) {
        $routeProvider.when('/trip/:username', {
            templateUrl: 'partials/trip/new-trip.html',
            controller: 'NewTripController',
            controllerAs: "newTripCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.user]
            }
        }).when('/:username/trip-list', {
            templateUrl: 'partials/trip/trip-list.html',
            controller: 'AllTripsController',
            controllerAs: "allTripsCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.user]
            }
        }).when('/trip/details/:id', {
            templateUrl: 'partials/trip/trip-details.html',
            controller: 'TripDetailsController',
            controllerAs: "tripDetailsCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.user]
            }
        })
    });

    tripsModule.controller('TripDetailsController', ['$scope', '$routeParams', 'TripFactory',
        function ($scope, $routeParams, TripFactory) {
            var tripId = $routeParams.id;
            var promise = TripFactory.findMarkers(tripId);
            promise.success(function (data) {
                $scope.markers = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });

            promise = TripFactory.findMapCentre(tripId);
            promise.success(function (data) {
                $scope.mapCentre = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });

            promise = TripFactory.findDescription(tripId);
            promise.success(function (data) {
                $scope.description = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });
        }
    ])
    ;

    tripsModule.controller('AllTripsController', ['$scope', '$routeParams', 'TripFactory',
        function ($scope, $routeParams, TripFactory) {
            var username = $routeParams.username;
            $scope.trips = [];
            var promise = TripFactory.findAll(username);
            promise.success(function (data) {
                $scope.trips = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });

        }]);

    tripsModule.controller('NewTripController', ['$scope', '$routeParams',
        function ($scope, $routeParams) {
            $scope.username = $routeParams.username;
        }]);

    tripsModule.controller('TripController', ['$scope', '$routeParams', '$route',
        'TripFactory', function ($scope, $routeParams, $route, TripFactory) {
            var self = this;
            self.submit = submit;
            self.username = $routeParams.username;
            self.trip = {
                id: null,
                city: '',
                arrivalTime: '',
                departureTime: '',
                amusement_park: false,
                art_gallery: false,
                aquarium: false,
                bakery: false,
                bar: false,
                cafe: false,
                cemetery: false,
                church: false,
                city_hall: false,
                gym: false,
                hindu_temple: false,
                library: false,
                movie_theater: false,
                museum: false,
                night_club: false,
                park: false,
                restaurant: false,
                shopping_mall: false,
                spa: false
            };
            function submit() {
                console.log('Saving New Trip', self.trip);
                console.log('Username', self.username);
                var promise = TripFactory.newTrip(self.username, self.trip);
                promise.success(function (data) {
                    $scope.msg = "Successfully created the trip!";
                    $route.reload();
                }).error(function (data, status, header, config) {
                    $scope.msg = "Could not create you trip :(";
                    alert(status);
                });
            }
        }]);

})
();
