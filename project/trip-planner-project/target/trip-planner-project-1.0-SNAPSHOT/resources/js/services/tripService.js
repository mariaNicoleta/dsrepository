(function () {
    var tripServices = angular.module('tripServices', []);

    tripServices.constant('config', {
        API_URL : 'http://localhost:8080/'
    })

    tripServices.factory('TripFactory', ['$http', 'config',
        function ($http, config) {

            var privateNewTrip = function (username, trip) {
                console.log(config.API_URL + '/trip/insert/' + username);
                console.log(trip);
                return $http.post(config.API_URL + '/trip/insert/' + username, trip);
            };

            var privateAllTrips = function (username) {
                return $http.get(config.API_URL + '/trip/' + username + '/all');
            };

            var privateTripMarkers = function (tripId) {
                return $http.get(config.API_URL + '/trip/markers/' + tripId);
            };

            var privateTripDescription = function (tripId) {
                return $http.get(config.API_URL + '/trip/description/' + tripId);
            };

            var privateTripMapCentre = function (tripId) {
                return $http.get(config.API_URL + '/trip/mapCentre/' + tripId);
            };

            return {
                newTrip: function (username, trip) {
                    return privateNewTrip(username, trip);
                },

                findAll: function (username) {
                    return privateAllTrips(username);
                },

                findMarkers: function (tripId) {
                    return privateTripMarkers(tripId);
                },

                findDescription: function (tripId) {
                    return privateTripDescription(tripId);
                },

                findMapCentre: function (tripId) {
                    return privateTripMapCentre(tripId);
                }
            };
        }]);

})();