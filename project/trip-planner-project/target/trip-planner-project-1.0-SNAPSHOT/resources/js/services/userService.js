(function () {
    var userServices = angular.module('userServices', []);

    userServices.constant('config', {
        API_URL : 'http://localhost:8080/'
    })

    userServices.factory('UserFactory', ['$http', 'config',
        function ($http, config) {

            var privateUserDetails = function (id) {
                return $http.get(config.API_URL + '/user/details/' + id);
            };

            var privateUserNotifications = function (username) {
                return $http.get(config.API_URL + 'notifications/' + username);
            };

            var privateUserList = function () {
                return $http.get(config.API_URL + '/user/all');
            };

            var privateReviewList = function () {
                return $http.get(config.API_URL + '/review/all');
            };

            var privateChartsList = function () {
                return $http.get(config.API_URL + '/review/charts');
            };

            var privateNewUser = function (user) {
                return $http.post(config.API_URL + '/user/new', user);
            };

            var privateNewReview = function (newrev) {
                return $http.post(config.API_URL + '/review/new', newrev);
            };

            var privateDeleteUser = function (id) {
                return $http.get(config.API_URL + '/user/delete/' + id);
            };

            return {
                findById: function (id) {
                    return privateUserDetails(id);
                },

                findAll: function () {
                    return privateUserList();
                },

                deleteUser: function () {
                    return privateDeleteUser(id);
                },

                newUser: function (user) {
                    return privateNewUser(user)
                },

                findReviews: function () {
                    return privateReviewList();
                },

                newReview: function (newrev) {
                    return privateNewReview(newrev)
                },

                getNotifications: function (username) {
                    return privateUserNotifications(username);
                },

                getCharts: function () {
                    return privateChartsList();
                },

                login: function (credentials) {
                    return $http
                        .post('/login', credentials)
                        .then(function (res) {
                            Session.create(res.data.id, res.data.user.id, res.data.user.role);
                            return res.data.user;
                        });
                },

                isAuthenticated: function () {
                    return !!Session.userId;
                },

                isAuthorized: function (authorizedRoles) {
                    if (!angular.isArray(authorizedRoles)) {
                        authorizedRoles = [authorizedRoles];
                    }
                    return (this.isAuthenticated() &&
                    authorizedRoles.indexOf(Session.userRole) !== -1);
                }

            };
        }]);

})();