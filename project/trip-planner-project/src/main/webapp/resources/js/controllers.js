'use strict';


myapp.controller('LoginController', function ($rootScope, $scope, $window, AuthSharedService, UserFactory) {
        $scope.login = function () {
            $rootScope.authenticationError = false;
            AuthSharedService.login(
                $scope.username,
                $scope.password
            );
        }
    })
    .controller('HomeController', function ($scope, HomeService) {
        $scope.technos = HomeService.getTechno();
    })
    .controller('UsersController', function ($scope, $log, $window, UserFactory) {
        var self = this;
        self.submit = submit;
        self.newuser = {
            id: 0,
            firstName: '',
            familyName: '',
            email: '',
            login: '',
            password: ''
        };

        function submit() {
            console.log('Saving New User', self.newuser);
            var promise = UserFactory.newUser(self.newuser);
            promise.success(function (data) {
                $scope.msg = "Successfully created the user!";
                $scope.users = UserFactory.findAll();
                confirm("Successfully created the user!");
                // $route.reload();
            }).error(function (data, status, header, config) {
                alert(status);
            });
        }

        self.deleteUser = deleteUser;
        function deleteUser(id) {
            console.log('Deleting user ', id);
            var promise = UserFactory.deleteUser(id);
            promise.success(function (data) {
                $scope.msg = "Successfully deleted the user!";
                $scope.users = UserFactory.findAll();
                $window.location.reload();
            }).error(function (data, status, header, config) {
                alert(status);
            });
        }
    })
    .controller('AdminController', function ($scope, UsersService) {
        // init form
        $scope.isLoading = false;
        $scope.url = $scope.swaggerUrl = 'v2/api-docs';
        // error management
        $scope.myErrorHandler = function (data, status) {
            console.log('failed to load swagger: ' + status + '   ' + data);
        };

        $scope.infos = false;
        $scope.users = UsersService.getAll();
        console.log("Getting them all");
    })
    .controller('LogoutController', function (AuthSharedService) {
        AuthSharedService.logout();
    })
    .controller('ErrorController', function ($scope, $routeParams) {
        $scope.code = $routeParams.code;

        switch ($scope.code) {
            case "403" :
                $scope.message = "Oops! you have come to unauthorised page."
                break;
            case "404" :
                $scope.message = "Page not found."
                break;
            default:
                $scope.code = 500;
                $scope.message = "Oops! unexpected error"
        }

    });