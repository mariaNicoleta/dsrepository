(function () {

    var usersModule = angular.module('userControllers', ['ngRoute', 'ng-fusioncharts'])

    usersModule.constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        user: 'user'
    });

    usersModule.config(function ($routeProvider, USER_ROLES) {
        $routeProvider.when('/users', {
            templateUrl: 'partials/user/user-list.html',
            controller: 'AllUsersController',
            controllerAs: "allUsersCtrl"
        }).when('/user/:id', {
            templateUrl: 'partials/user/user-details.html',
            controller: 'UserController',
            controllerAs: "userCtrl"
        }).when('/userMain/:username', {
            templateUrl: 'partials/user/user.html',
            controller: 'UserMainPageController',
            controllerAs: "userMainPageCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.user]
            }
        }).when('/reviews/:username', {
            templateUrl: 'partials/review/review-list.html',
            controller: 'ReviewController',
            controllerAs: "reviewCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.user]
            }
        }).when('/charts', {
            templateUrl: 'partials/admin/charts.html',
            controller: 'ChartsController',
            controllerAs: "chartsCtrl",
            access: {
                loginRequired: true,
                authorizedRoles: [USER_ROLES.admin]
            }
        })

    });

    usersModule.controller('AllUsersController', ['$scope', 'UserFactory',
        function ($scope, UserFactory) {
            $scope.users = [];
            var promise = UserFactory.findAll();
            promise.success(function (data) {
                $scope.users = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });

        }]);

    usersModule.controller('UserController', ['$scope', '$routeParams',
        'UserFactory', function ($scope, $routeParams, UserFactory) {
            var id = $routeParams.username;
            var promise = UserFactory.findById(id);
            $scope.user = null;
            promise.success(function (data) {
                $scope.user = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });
        }]);

    usersModule.controller('UserMainPageController', ['$scope', '$routeParams',
        'UserFactory', function ($scope, $routeParams, UserFactory) {
            var username = $routeParams.username;
            var promise = UserFactory.getNotifications(username);
            $scope.notifications = [];
            promise.success(function (data) {
                $scope.notifications = data;
            }).error(function (data, status, header, config) {
                alert(status);
            });
        }]);

    usersModule.controller('ReviewController', ['$scope', '$routeParams', 'UserFactory',
        function ($scope, $routeParams, UserFactory) {
            $scope.reviews = [];
            var promise = UserFactory.findReviews();
            promise.success(function (data) {
                $scope.reviews = data;
                $scope.sortType = 'title'; // set the default sort type
                $scope.sortReverse = false;  // set the default sort order
                $scope.searchTerm = '';     // set the default search/filter term
            }).error(function (data, status, header, config) {
                alert(status);
            });

            var self = this;
            self.newrev = {
                title: '',
                username: $routeParams.username,
                content: ''
            };
            self.submit = submit;
            function submit() {
                console.log('Saving New Review', self.newrev);
                var promise = UserFactory.newReview(self.newrev);
                promise.success(function (data) {
                    $scope.msg = "Successfully created the review!";
                    $route.reload();
                }).error(function (data, status, header, config) {
                    $scope.msg = "Could not create the review :(";
                    alert(status);
                });
            }
        }]);

    usersModule.controller('ChartsController', ['$scope', 'UserFactory',
        function ($scope, UserFactory) {
            $scope.myDataSource = {chart: {}, data: []};
            var promise = UserFactory.getCharts();
            promise.success(function (data) {
                console.log("Yey");
                $scope.myDataSource = {
                    chart: {
                        caption: "Most visited places",
                        subcaption: "Here is your report",
                        startingangle: "120",
                        showlabels: "0",
                        showlegend: "1",
                        enablemultislicing: "0",
                        slicingdistance: "15",
                        showpercentvalues: "1",
                        showpercentintooltip: "0",
                        plottooltext: "Place : $label Total times visited : $datavalue",
                        theme: "fint"
                    },
                    data: data
                };
                console.log($scope.myDataSource);
                $route.reload();
            }).error(function (data, status, header, config) {
                alert(status);
            });

        }]);
})();
