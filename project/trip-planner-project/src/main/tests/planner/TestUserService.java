package planner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import planner.service.UserServiceImpl;
import planner.service.dto.UserDTO;

import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class TestUserService {

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void testCreate() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(0L);
        userDTO.setEmail("mail@mail.com");
        userDTO.setEnabled(true);
        userDTO.setFamilyName("Hayrick");
        userDTO.setFirstName("Hardy");
        userDTO.setLogin("hardy");
        userDTO.setPassword("secret");
        userService.save(userDTO);

        UserDTO userFromDB = userService.findOne(1L);
        assert(userFromDB.getLogin().equals(userDTO.getLogin()));
        assertNotNull(userFromDB);
    }

}