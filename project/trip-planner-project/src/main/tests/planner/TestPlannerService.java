package planner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import planner.model.place.PlaceDetails;
import planner.service.dto.TripDTO;
import planner.service.PlannerService;

import java.util.Calendar;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestJPAConfiguration.class })
@Transactional
public class TestPlannerService {

    @Autowired
    PlannerService plannerService;

    @Test
    public void testBuildItinerary() throws Exception {
        TripDTO trip = new TripDTO();
        trip.setCity("Paris");
        Calendar instance = Calendar.getInstance();
        trip.setArrivalTime(instance.getTime());
        instance.add(Calendar.DAY_OF_WEEK, 3);
        trip.setDepartureTime(instance.getTime());
        trip.setMuseum(true);
        trip.setPark(true);
        plannerService.createItinerary(trip, 0L);
    }

}
