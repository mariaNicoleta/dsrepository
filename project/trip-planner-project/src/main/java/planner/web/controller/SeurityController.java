package planner.web.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import planner.persist.entity.User;
import planner.persist.repo.UserRepo;
import planner.security.SecurityUtils;

@RestController
@Api(description = "Users management API")
public class SeurityController {


    @Autowired
    private UserRepo userRepo;

    @RequestMapping(value = "/security/account", method = RequestMethod.GET)
    public
    @ResponseBody
    User getUserAccount() {
        User user = userRepo.findByLogin(SecurityUtils.getCurrentLogin());
        user.setPassword(null);
        return user;
    }
}
