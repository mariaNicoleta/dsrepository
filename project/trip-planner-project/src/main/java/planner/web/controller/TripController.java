package planner.web.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import planner.persist.entity.Trip;
import planner.persist.entity.Trip_details;
import planner.persist.entity.User;
import planner.persist.repo.TripDetailsRepo;
import planner.persist.repo.TripRepo;
import planner.persist.repo.UserRepo;
import planner.service.PlannerService;
import planner.service.TripService;
import planner.service.dto.TripDTO;
import planner.service.utils.MailHelper;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/trip")
@Api
public class TripController {

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private UserRepo userService;

    @Autowired
    private TripService tripService;

    MailHelper mailHelper = new MailHelper("samSmithTestMail@gmail.com", "samsmithabc125");

    @RequestMapping(value = "/insert/{username}", method = RequestMethod.POST)
    public boolean insertTrip(@PathVariable("username") String username, @RequestBody TripDTO tripDTO) {
        User user = userService.findByLogin(username);
        boolean itineraryCreated = plannerService.createItinerary(tripDTO, user.getId());
        if (itineraryCreated) {
            mailHelper.sendMail(user.getEmail(), "Trip to " + tripDTO.getCity() + "!", tripDTO.toString());
            return true;
        }
        return false;
    }

    ///trips/' + username + '/all
    @RequestMapping(value = "/{username}/all", method = RequestMethod.GET)
    public List<Trip> findAllTrips(@PathVariable("username") String username) {
        User user = userService.findByLogin(username);
        return tripService.findTripByUserid(user.getId());
    }

    @RequestMapping(value = "/markers/{tripId}", method = RequestMethod.GET)
    public Map<Double, Double> getTripMarkers(@PathVariable("tripId") int tripId) {
        Map<Double, Double> markers = new LinkedHashMap<Double, Double>();
        List<Trip_details> detailsList = tripService.findTripDetails((long) tripId);
        for(Trip_details details : detailsList) {
            markers.put(details.getLat(), details.getLng());
        }
        return markers;
    }

    @RequestMapping(value = "/mapCentre/{tripId}", method = RequestMethod.GET)
    public Map<Double, Double> getTripMapCentre(@PathVariable("tripId") int tripId) {
        Map<Double, Double> markers = new LinkedHashMap<Double, Double>();
        Trip trip = tripService.findTripByTripId((long) tripId);
        markers.put(trip.getLat(), trip.getLng());
        return markers;
    }

    @RequestMapping(value = "/description/{tripId}", method = RequestMethod.GET)
    public Map<String, Date> getTripDescription(@PathVariable("tripId") int tripId) {
        Map<String, Date> descr = new LinkedHashMap<String, Date>();
        List<Trip_details> detailsList = tripService.findTripDetails((long) tripId);
        for(Trip_details details : detailsList) {
            descr.put(details.getName(), details.getDate());
        }
        return descr;
    }
}

