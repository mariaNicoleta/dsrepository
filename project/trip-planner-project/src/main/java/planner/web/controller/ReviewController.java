package planner.web.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import planner.model.trip.ChartData;
import planner.persist.entity.Reviews;
import planner.persist.entity.Trip;
import planner.service.TripService;
import planner.service.dto.ReviewDTO;

import java.util.*;

@RestController
@RequestMapping("/review")
@Api
public class ReviewController {

    @Autowired
    private TripService tripService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ReviewDTO> getAllReviews() {
        List<Reviews> reviewsList = tripService.getReviews();
        List<ReviewDTO> reviews = new ArrayList<ReviewDTO>();
        for(Reviews currentReview : reviewsList) {
            reviews.add(new ReviewDTO(
                    currentReview.getTitle(),
                    currentReview.getUsername(),
                    currentReview.getContent()));
        }
        return reviews;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public
    @ResponseBody
    Reviews saveReview(@RequestBody Reviews reviews) {
        return tripService.saveReview(reviews);
    }

    @RequestMapping(value = "/charts", method = RequestMethod.GET)
    public List<ChartData> getMostVisitedPlaces() {
        List<ChartData> chartDataList = new ArrayList<ChartData>();
        List<Trip> allTrips = tripService.getAllTrips();
        Map<String, Integer> frequency = new HashMap<String, Integer>();
        List<String> cityList = new ArrayList<String>();
        for(Trip trip : allTrips) {
            cityList.add(trip.getCity());
        }
        Set<String> citySet = new HashSet<String>(cityList);
        for(String city : citySet) {
            int frequency1 = Collections.frequency(cityList, city);
            chartDataList.add(new ChartData(city, (double) frequency1));
        }
        return chartDataList;
    }
}

