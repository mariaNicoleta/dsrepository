package planner.web.controller;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import planner.persist.entity.Trip;
import planner.persist.entity.Trip_details;
import planner.persist.entity.User;
import planner.persist.entity.UsersAuthority;
import planner.persist.repo.UserAuthRepo;
import planner.persist.repo.UserRepo;
import planner.service.TripService;
import planner.service.UserServiceImpl;
import planner.service.dto.UserDTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@Api(description = "Users management API")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TripService tripService;

    @Autowired
    private UserAuthRepo userAuthRepo;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public
    @ResponseBody
    List<UserDTO> usersList() {
        logger.debug("get users list");
        return userService.findAll();
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public
    @ResponseBody
    UserDTO getUser(@PathVariable Long userId) {
        logger.debug("get user");
        return userService.findOne(userId);
    }

    @RequestMapping(value = "/user/delete/{userId}", method = RequestMethod.GET)
    public void deleteUser(@PathVariable Long userId) {
        userService.delete(userId);
    }

    @RequestMapping(value = "/user/new", method = RequestMethod.POST)
    public
    @ResponseBody
    User saveUser(@RequestBody UserDTO user) {
        logger.debug("save user");
        user.setEnabled(true);
        User savedUser = userService.save(user);
        saveUserAuth(savedUser.getId());
        return savedUser;
    }

    private void saveUserAuth(long userId) {
        UsersAuthority authority = new UsersAuthority();
        authority.setId_user(userId);
        authority.setId_authority(2L);
        userAuthRepo.save(authority);
    }

    @RequestMapping(value = "/notifications/{username}", method = RequestMethod.GET)
    public List<String> getUserNotifications(@PathVariable("username") String username) {
        List<String> notifications = new ArrayList<String>();
        User user = userRepo.findByLogin(username);
        List<Trip> tripList = tripService.findTripByUserid(user.getId());
        Trip currentTrip = null;
        Calendar instance = Calendar.getInstance();
        long currentTime = instance.getTime().getTime();
        for (Trip trip : tripList) {
            if (trip.getArrivaltime().getTime() < currentTime
                    && currentTime < trip.getDeparturetime().getTime()) {
                currentTrip = trip;
                break;
            }
        }
        if (currentTrip != null) {
            // get trip details from this trip
            List<Trip_details> tripDetailsList = tripService.findTripDetails(currentTrip.getId());
            for (Trip_details tripDetails : tripDetailsList) {
                notifications.add(tripDetails.getDate() + "\t\t\t" + tripDetails.getName());
            }
        } else {
            notifications.add("You have no ongoing trips!");
        }
        return notifications;
    }
}

 
