package planner.model.place;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceDetailsResponse {
    @JsonProperty("results")
    private List<PlaceDetails> result;

    public List<PlaceDetails> getResult() {
        return result;
    }

    public void setResult(List<PlaceDetails> result) {
        this.result = result;
    }
}