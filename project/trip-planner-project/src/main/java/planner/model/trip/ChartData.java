package planner.model.trip;

/**
 * Created by maria on 1/7/2017.
 */
public class ChartData {

    private String label;
    private Double value;

    public ChartData(String label, Double value) {
        this.label = label;
        this.value = value;
    }

    public ChartData() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
