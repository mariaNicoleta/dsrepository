package planner.model.trip;

import java.util.Date;

/**
 * Created by maria on 1/6/2017.
 */
public class ScheduleEntry {
    Date time;
    String description;

    public ScheduleEntry(Date time, String description) {
        this.time = time;
        this.description = description;
    }
}
