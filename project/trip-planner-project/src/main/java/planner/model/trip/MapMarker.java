package planner.model.trip;

public class MapMarker {
    double latitude;
    double longitude;

    public MapMarker(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
