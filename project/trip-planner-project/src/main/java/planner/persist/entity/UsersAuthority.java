package planner.persist.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "users_authority")
public class UsersAuthority {

  @Id
  @GenericGenerator(name = "generator", strategy = "increment")
  @GeneratedValue(generator = "generator")
  private Long id;

  @Column(name = "id_user", nullable = false)
  private Long id_user;

  @Column(name = "id_authority", nullable = false)
  private Long id_authority;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_user() {
    return id_user;
  }

  public void setId_user(Long id_user) {
    this.id_user = id_user;
  }

  public Long getId_authority() {
    return id_authority;
  }

  public void setId_authority(Long id_authority) {
    this.id_authority = id_authority;
  }
}
