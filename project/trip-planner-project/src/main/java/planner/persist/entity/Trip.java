package planner.persist.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "trip")
public class Trip {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "userId", nullable = false)
    private Long userid;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "arrivaltime", nullable = false)
    private java.sql.Date arrivaltime;

    @Column(name = "departuretime", nullable = false)
    private java.sql.Date departuretime;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @Column(name = "lng", nullable = false)
    private Double lng;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public java.sql.Date getArrivaltime() {
        return arrivaltime;
    }

    public void setArrivaltime(java.sql.Date arrivaltime) {
        this.arrivaltime = arrivaltime;
    }

    public java.sql.Date getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(java.sql.Date departuretime) {
        this.departuretime = departuretime;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
