package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import planner.persist.entity.Trip_details;

import java.util.List;

public interface TripDetailsRepo extends JpaRepository<Trip_details, Long> {

    List<Trip_details> findByTripid(long tripId);
}
