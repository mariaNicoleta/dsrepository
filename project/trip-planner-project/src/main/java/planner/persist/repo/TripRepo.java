package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import planner.persist.entity.Trip;
import planner.service.dto.TripDTO;

import java.util.List;

public interface TripRepo extends JpaRepository<Trip, Long>{
    List<Trip> findByUserid(long userId);
}
