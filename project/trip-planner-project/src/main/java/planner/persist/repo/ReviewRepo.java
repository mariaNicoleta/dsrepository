package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import planner.persist.entity.Reviews;

public interface ReviewRepo extends JpaRepository<Reviews, Long>{
}
