package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import planner.persist.entity.Authority;

public interface AuthorityRepo extends JpaRepository<Authority, Long> {

}