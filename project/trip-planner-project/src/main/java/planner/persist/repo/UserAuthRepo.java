package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import planner.persist.entity.UsersAuthority;

public interface UserAuthRepo extends JpaRepository<UsersAuthority, Long> {
}
