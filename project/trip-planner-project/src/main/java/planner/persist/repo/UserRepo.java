package planner.persist.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import planner.persist.entity.User;


public interface UserRepo extends JpaRepository<User, Long> {
    User findByLogin(String login);

}
