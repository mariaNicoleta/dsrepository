package planner.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("planner.service")
public class ServiceConfig {
   
     

}
