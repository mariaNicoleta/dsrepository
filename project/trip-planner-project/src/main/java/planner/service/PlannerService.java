package planner.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import planner.model.place.PlaceDetails;
import planner.model.place.PlaceDetailsResponse;
import planner.model.place.PlaceLocation;
import planner.model.place.PlacePhoto;
import planner.persist.entity.Trip;
import planner.persist.entity.Trip_details;
import planner.persist.repo.TripDetailsRepo;
import planner.persist.repo.TripRepo;
import planner.service.dto.TripDTO;

import java.util.*;

@Service
public class PlannerService {

    private static final RestTemplate restTemplate = new RestTemplate();

    private static final String PLACES_API = "https://maps.googleapis.com/maps/api/place/nearbysearch/";
    ;
    private static final String GEOCODE_API = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    private static final String API_KEY = "AIzaSyA7V1XrguiMXskgnL94gCOTlmrrGi9o4go";

    @Autowired
    private TripRepo tripRepo;

    @Autowired
    private TripDetailsRepo tripDetailsRepo;

    public boolean createItinerary(TripDTO trip, long userId) {

        String city = trip.getCity();
        PlaceLocation cityLocation = getLatLong(city);
        if (cityLocation == null) {
            return false;
        }

        final String latitude = cityLocation.getLat();
        final String longitude = cityLocation.getLng();
        final List<String> keywords = getKeyWords(trip);
        List<PlaceDetails> placeDetailsList = new ArrayList<PlaceDetails>();

        for(String keyword : keywords) {
            PlaceDetailsResponse response = restTemplate
                    .getForObject(PLACES_API +
                            "json?location=" + latitude + "," + longitude +
                            "&radius=500" +
                            "&rankby=prominence" +
                            //"&types=" + placeType +
                            "&keyword=" + keyword +
                            "&key=" + API_KEY, PlaceDetailsResponse.class);
            if(response != null) {
                placeDetailsList.addAll(response.getResult());
            }
        }

        if (!placeDetailsList.isEmpty()) {
            processTrip(placeDetailsList, trip, userId, latitude, longitude);
            return true;
        }
        return false;
    }

    private List<String> getKeyWords(TripDTO trip) {
        StringBuilder builder = new StringBuilder();
        List<String> keywords = new ArrayList<String>();
        if(trip.isAmusement_park()) {
            keywords.add("amusement_park");
        }
        if(trip.isAquarium()) {
            keywords.add("aquarium");
        }
        if(trip.isArt_gallery()) {
            keywords.add("art_gallery");
        }
        if(trip.isBakery()) {
            keywords.add("bakery");
        }
        if(trip.isBar()) {
            keywords.add("bar");
        }
        if(trip.isCafe()) {
            keywords.add("cafe");
        }
        if(trip.isCemetery()) {
            keywords.add("cemetery");
        }
        if(trip.isChurch()) {
            keywords.add("church");
        }
        if(trip.isCity_hall()) {
            keywords.add("city_hall");
        }
        if(trip.isGym()) {
            keywords.add("gym");
        }
        if(trip.isHindu_temple()) {
            keywords.add("hindu_temple");
        }
        if(trip.isLibrary()) {
            keywords.add("library");
        }
        if(trip.isMovie_theater()) {
            keywords.add("movie_theater");
        }
        if(trip.isNight_club()) {
            keywords.add("night_club");
        }
        if(trip.isPark()) {
            keywords.add("park");
        }
        if(trip.isMuseum()) {
            keywords.add("museum");
        }
        if(trip.isRestaurant()) {
            keywords.add("restaurant");
        }
        if(trip.isShopping_mall()) {
            keywords.add("shopping_mall");
        }
        if(trip.isSpa()) {
            keywords.add("spa");
        }
        return keywords;
    }

    /**
     * Saves it as well.
     */
    private void processTrip(List<PlaceDetails> placeDetailsList, TripDTO trip, long userId, String latitude, String longitude) {
        Date arrival = trip.getArrivalTime();
        Date departure = trip.getDepartureTime();
        int nrDays = daysBetween(arrival, departure) + 1; // also the arrival and departure day count as one
        int nrObjectives = 3;
        if (nrDays > 1) {
            nrObjectives = nrDays * 3;
        }
        trip.setLat(Double.parseDouble(latitude));
        trip.setLng(Double.parseDouble(longitude));
        saveTrip(trip, userId, arrival, departure);


        List<PlaceDetails> finalPlaceDetails = new ArrayList<PlaceDetails>();
        if(placeDetailsList.size() >= nrObjectives) {
            Collections.shuffle(placeDetailsList, new Random(System.nanoTime()));
            finalPlaceDetails = placeDetailsList.subList(0, nrObjectives);
        } else {
            finalPlaceDetails = placeDetailsList;
        }
        processTripDetails(finalPlaceDetails, trip.getId(), arrival);
    }

    private void saveTrip(TripDTO trip, long userId, Date arrival, Date departure) {
        Trip entity = new Trip();
        entity.setUserid(userId);
        entity.setCity(trip.getCity());
        entity.setArrivaltime(toSqlDate(arrival));
        entity.setDeparturetime(toSqlDate(departure));
        entity.setLat(trip.getLat());
        entity.setLng(trip.getLng());
        Trip savedTrip = tripRepo.save(entity);
        trip.setId(Integer.parseInt(savedTrip.getId().toString()));
    }

    private java.sql.Date toSqlDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return new java.sql.Date(calendar.getTimeInMillis());
    }

    /**
     * Saves it as well.
     */
    private void processTripDetails(List<PlaceDetails> placeDetails, long tripId, Date arrivalTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(arrivalTime);
        getNextVisitingTime(cal);
        for (PlaceDetails place : placeDetails) {
            String name = place.getName();

            PlaceLocation location = place.getGeometry().getLocation();
            Double lat = Double.parseDouble(location.getLat());
            Double lng = Double.parseDouble(location.getLng());

            java.sql.Date nextVisitingTime = new java.sql.Date(cal.getTimeInMillis());
            cal = getNextVisitingTime(cal);

            List<PlacePhoto> photos = place.getPhotos();
            String photoRef = "";
            if (photos != null && !photos.isEmpty()) {
                photoRef = photos.get(0).getReference();
            }

            saveTripDetails(tripId, name, lat, lng, nextVisitingTime, photoRef);
        }
    }

    private void saveTripDetails(long tripId, String name, Double lat, Double lng,
                                 java.sql.Date nextVisitingTime, String photoRef) {
        Trip_details tripDetails = new Trip_details();
        tripDetails.setTripid(tripId);
        tripDetails.setLng(lng);
        tripDetails.setLat(lat);
        tripDetails.setDate(nextVisitingTime);
        if (!photoRef.isEmpty() && photoRef.length() < 140) {
            tripDetails.setPhoto(photoRef);
        }
        tripDetails.setName(name);
        tripDetailsRepo.save(tripDetails);
    }

    private Calendar getNextVisitingTime(Calendar cal) {
        cal.add(Calendar.HOUR_OF_DAY, 3);
        int timeOfDay = cal.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 21 && timeOfDay < 24) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.HOUR_OF_DAY, 9);
        }
        return cal;
    }

    private int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    private PlaceLocation getLatLong(String city) {
        Map response = restTemplate.getForObject(GEOCODE_API + city + "&key=" + API_KEY, Map.class);
        Object status = response.get("status");
        if ("OK".equalsIgnoreCase((String) status)) {
            response.get("results");
            Map locationResults = (Map) ((Map) ((Map) ((List) response.get("results")).get(0)).get("geometry")).get("location");
            Double lat = (Double) locationResults.get("lat");
            Double lng = (Double) locationResults.get("lng");
            PlaceLocation placeLocation = new PlaceLocation();
            placeLocation.setLat(String.valueOf(lat));
            placeLocation.setLng(String.valueOf(lng));
            return placeLocation;
        }
        return null;
    }
}
