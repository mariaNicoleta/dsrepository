package planner.service;

import org.springframework.stereotype.Service;

import planner.persist.entity.Authority;
import planner.service.dto.AuthorityDTO;

@Service
public class AuthorityServiceImpl extends GenericServiceImpl<Authority, AuthorityDTO, Long>{

    
}
