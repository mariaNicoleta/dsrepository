package planner.service.dto;

public class ReviewDTO {

    private String title;
    private String city;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ReviewDTO() {

    }

    public ReviewDTO(String title, String city, String content) {

        this.title = title;
        this.city = city;
        this.content = content;
    }
}