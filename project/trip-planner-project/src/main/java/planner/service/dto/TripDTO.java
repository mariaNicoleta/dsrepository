package planner.service.dto;

import java.util.Date;

public class TripDTO {

    private Integer id;
    private String city;
    private Date arrivalTime;
    private Date departureTime;
    private boolean amusement_park;
    private boolean art_gallery;
    private boolean aquarium;
    private boolean bakery;
    private boolean bar;
    private boolean cafe;
    private boolean cemetery;
    private boolean church;
    private boolean city_hall;
    private boolean gym;
    private boolean hindu_temple;
    private boolean library;
    private boolean movie_theater;
    private boolean museum;
    private boolean night_club;
    private boolean park;
    private boolean restaurant;
    private boolean shopping_mall;
    private boolean spa;
    private double lat;
    private double lng;

    public TripDTO() {
    }

    public TripDTO(Integer id, String city, Date arrivalTime, Date departureTime, boolean amusement_park, boolean art_gallery, boolean aquarium, boolean bakery, boolean bar, boolean cafe, boolean cemetery, boolean church, boolean city_hall, boolean gym, boolean hindu_temple, boolean library, boolean movie_theater, boolean museum, boolean night_club, boolean park, boolean restaurant, boolean shopping_mall, boolean spa, double lat, double lng) {
        this.id = id;
        this.city = city;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.amusement_park = amusement_park;
        this.art_gallery = art_gallery;
        this.aquarium = aquarium;
        this.bakery = bakery;
        this.bar = bar;
        this.cafe = cafe;
        this.cemetery = cemetery;
        this.church = church;
        this.city_hall = city_hall;
        this.gym = gym;
        this.hindu_temple = hindu_temple;
        this.library = library;
        this.movie_theater = movie_theater;
        this.museum = museum;
        this.night_club = night_club;
        this.park = park;
        this.restaurant = restaurant;
        this.shopping_mall = shopping_mall;
        this.spa = spa;
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public boolean isAmusement_park() {
        return amusement_park;
    }

    public void setAmusement_park(boolean amusement_park) {
        this.amusement_park = amusement_park;
    }

    public boolean isArt_gallery() {
        return art_gallery;
    }

    public void setArt_gallery(boolean art_gallery) {
        this.art_gallery = art_gallery;
    }

    public boolean isAquarium() {
        return aquarium;
    }

    public void setAquarium(boolean aquarium) {
        this.aquarium = aquarium;
    }

    public boolean isBakery() {
        return bakery;
    }

    public void setBakery(boolean bakery) {
        this.bakery = bakery;
    }

    public boolean isBar() {
        return bar;
    }

    public void setBar(boolean bar) {
        this.bar = bar;
    }

    public boolean isCafe() {
        return cafe;
    }

    public void setCafe(boolean cafe) {
        this.cafe = cafe;
    }

    public boolean isCemetery() {
        return cemetery;
    }

    public void setCemetery(boolean cemetery) {
        this.cemetery = cemetery;
    }

    public boolean isChurch() {
        return church;
    }

    public void setChurch(boolean church) {
        this.church = church;
    }

    public boolean isCity_hall() {
        return city_hall;
    }

    public void setCity_hall(boolean city_hall) {
        this.city_hall = city_hall;
    }

    public boolean isGym() {
        return gym;
    }

    public void setGym(boolean gym) {
        this.gym = gym;
    }

    public boolean isHindu_temple() {
        return hindu_temple;
    }

    public void setHindu_temple(boolean hindu_temple) {
        this.hindu_temple = hindu_temple;
    }

    public boolean isLibrary() {
        return library;
    }

    public void setLibrary(boolean library) {
        this.library = library;
    }

    public boolean isMovie_theater() {
        return movie_theater;
    }

    public void setMovie_theater(boolean movie_theater) {
        this.movie_theater = movie_theater;
    }

    public boolean isMuseum() {
        return museum;
    }

    public void setMuseum(boolean museum) {
        this.museum = museum;
    }

    public boolean isNight_club() {
        return night_club;
    }

    public void setNight_club(boolean night_club) {
        this.night_club = night_club;
    }

    public boolean isPark() {
        return park;
    }

    public void setPark(boolean park) {
        this.park = park;
    }

    public boolean isRestaurant() {
        return restaurant;
    }

    public void setRestaurant(boolean restaurant) {
        this.restaurant = restaurant;
    }

    public boolean isShopping_mall() {
        return shopping_mall;
    }

    public void setShopping_mall(boolean shopping_mall) {
        this.shopping_mall = shopping_mall;
    }

    public boolean isSpa() {
        return spa;
    }

    public void setSpa(boolean spa) {
        this.spa = spa;
    }

    /*public static class Builder {
        private Integer id;
        private String city;
        private Date arrivalTime;
        private Date departureTime;
        private boolean amusement_park;
        private boolean art_gallery;
        private boolean aquarium;
        private boolean bakery;
        private boolean bar;
        private boolean cafe;
        private boolean cemetery;
        private boolean church;
        private boolean city_hall;
        private boolean gym;
        private boolean hindu_temple;
        private boolean library;
        private boolean movie_theater;
        private boolean museum;
        private boolean night_club;
        private boolean park;
        private boolean restaurant;
        private boolean shopping_mall;
        private boolean spa;

        public Builder id(int id) {
            this.nestedid = id;
            return this;
        }

        public Builder firstname(String name) {
            this.nestedfirstname = name;
            return this;
        }

        public Builder surname(String name) {
            this.nestedsurname = name;
            return this;
        }

        public Builder email(String email) {
            this.nestedemail = email;
            return this;
        }

        public Builder address(String address) {
            this.nestedaddress = address;
            return this;
        }

        public Builder postcode(String postcode) {
            this.nestedpostcode = postcode;
            return this;
        }

        public Builder city(String city) {
            this.nestedcity = city;
            return this;
        }

        public Builder country(String country) {
            this.nestedcountry = country;
            return this;
        }

        public Builder telephone(String telephone) {
            this.nestedtelephone = telephone;
            return this;
        }

        public Builder IBAN(String iban) {
            this.nestedIBAN = iban;
            return this;
        }


        public TripDTO create() {
            return new TripDTO(nestedid, nestedfirstname, nestedsurname, nestedemail, nestedaddress, nestedpostcode,
                    nestedcity, nestedcountry, nestedtelephone, nestedIBAN);
        }

    }*/

}
