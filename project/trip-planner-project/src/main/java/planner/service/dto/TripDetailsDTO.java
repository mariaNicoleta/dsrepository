package planner.service.dto;

import planner.model.trip.MapMarker;
import planner.model.trip.ScheduleEntry;

import java.util.List;

/**
 * Created by maria on 1/6/2017.
 */
public class TripDetailsDTO {

    List<MapMarker> markers;
    MapMarker mapCenter;
    List<ScheduleEntry> description;

    public TripDetailsDTO() {
    }

    public TripDetailsDTO(List<MapMarker> markers, MapMarker mapCenter, List<ScheduleEntry> description) {
        this.markers = markers;
        this.mapCenter = mapCenter;
        this.description = description;
    }

    public List<MapMarker> getMarkers() {
        return markers;
    }

    public void setMarkers(List<MapMarker> markers) {
        this.markers = markers;
    }

    public MapMarker getMapCenter() {
        return mapCenter;
    }

    public void setMapCenter(MapMarker mapCenter) {
        this.mapCenter = mapCenter;
    }

    public List<ScheduleEntry> getDescription() {
        return description;
    }

    public void setDescription(List<ScheduleEntry> description) {
        this.description = description;
    }
}
