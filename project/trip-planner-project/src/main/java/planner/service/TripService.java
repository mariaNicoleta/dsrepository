package planner.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import planner.persist.entity.Reviews;
import planner.persist.entity.Trip;
import planner.persist.entity.Trip_details;
import planner.persist.repo.ReviewRepo;
import planner.persist.repo.TripDetailsRepo;
import planner.persist.repo.TripRepo;

import java.util.List;

@Service
public class TripService {

    @Autowired
    private TripRepo tripRepo;

    @Autowired
    private TripDetailsRepo tripDetailsRepo;

    @Autowired
    private ReviewRepo reviewRepo;


    public List<Trip> findTripByUserid(long id) {
        return tripRepo.findByUserid(id);
    }

    public List<Trip_details> findTripDetails(Long id) {
        return tripDetailsRepo.findByTripid(id);
    }

    public Trip findTripByTripId(long tripId) {
        return tripRepo.findOne(tripId);
    }

    public List<Trip> getAllTrips() {
        return tripRepo.findAll();
    }

    public List<Reviews> getReviews() { return reviewRepo.findAll(); }

    public Reviews saveReview(Reviews reviews) {
        return reviewRepo.save(reviews);
    }
}
