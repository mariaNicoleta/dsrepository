package planner.service;

import org.springframework.stereotype.Service;
import planner.persist.entity.User;
import planner.service.dto.UserDTO;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, UserDTO, Long>{

}
