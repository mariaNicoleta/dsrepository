﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;

namespace TestWS
{
    public class DatabaseService
    {
        string MyConnection = "datasource=localhost;"
            + "port=3306;"
            + "username=root;"
            + "password=root";

        public void InsertPackage(PackageEntity packageEntity)
        {
            try
            {
                string Query = "insert into `assignment-four-db`.packages"
                    + "(id, sender_id, receriver_id, name, description,"
                    + "sender_city, destination_city, tracking) values("
                    + "'" + packageEntity.Id
                    + "','" + packageEntity.SenderId
                    + "','" + packageEntity.ReceiverId
                    + "','" + packageEntity.Name
                    + "','" + packageEntity.Description
                    + "','" + packageEntity.SenderCity
                    + "','" + packageEntity.DestinationCity
                    + "','" + packageEntity.Tracking
                    + "');";
                MySqlConnection MySqlConnection = new MySqlConnection(MyConnection);
                MySqlCommand MyCommand = new MySqlCommand(Query, MySqlConnection);
                MySqlDataReader MyReader;
                MySqlConnection.Open();
                MyReader = MyCommand.ExecuteReader();
                MySqlConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Insert exception: {0}", ex.Message);
            }
        }

        public void DeletePackage(long id)
        {
            Console.WriteLine("Delete Request: {0}\n", id);
            try
            {
                string Query = "delete from `assignment-four-db`.packages"
                    + "where id = "
                    + "sender_city_id, destination_city_id, tracking) values("
                    + "'" + id
                    + "';";
                MySqlConnection MySqlConnection = new MySqlConnection(MyConnection);
                MySqlCommand MyCommand = new MySqlCommand(Query, MySqlConnection);
                MySqlDataReader MyReader;
                MySqlConnection.Open();
                MyReader = MyCommand.ExecuteReader();
                MySqlConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete exception: {0}", ex.Message);
            }

        }
    }
}