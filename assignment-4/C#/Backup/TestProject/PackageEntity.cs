﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace TestWS
{
    public class PackageEntity
    {
        public long Id { get; set; }
        public long SenderId { get; set; }
        public long ReceiverId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SenderCity { get; set; }
        public string DestinationCity { get; set; }
        public bool Tracking { get; set; }
    }
}