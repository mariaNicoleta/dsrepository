﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using TestWS;

namespace TestProject
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("WSDLToWebService", "1.0.0.0")]
    [System.Web.Services.WebServiceAttribute(Namespace = "http://softwarebydefault.com")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "TestWebService", Namespace = "http://softwarebydefault.com")]
    public class TestWebService : System.Web.Services.WebService
    {
        private DatabaseService db = new DatabaseService();

        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://softwarebydefault.com/addPackage",
        RequestNamespace = "http://softwarebydefault.com", ResponseNamespace = "http://softwarebydefault.com",
        Use = System.Web.Services.Description.SoapBindingUse.Literal,
        ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void addPackage(PackageEntity packageEntity)
        {
            db.InsertPackage(packageEntity);
        }

        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://softwarebydefault.com/removePackage",
        RequestNamespace = "http://softwarebydefault.com", ResponseNamespace = "http://softwarebydefault.com",
        Use = System.Web.Services.Description.SoapBindingUse.Literal,
        ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void removePackage(long id)
        {
            db.DeletePackage(id);
        }

        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://softwarebydefault.com/registerForTracking",
        RequestNamespace = "http://softwarebydefault.com", ResponseNamespace = "http://softwarebydefault.com",
        Use = System.Web.Services.Description.SoapBindingUse.Literal,
        ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void registerForTracking(PackageEntity packageEntity)
        {

        }

        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://softwarebydefault.com/addRoute",
        RequestNamespace = "http://softwarebydefault.com", ResponseNamespace = "http://softwarebydefault.com",
        Use = System.Web.Services.Description.SoapBindingUse.Literal,
        ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void addRoute(String city, DateTime time, long packageId)
        {

        }
    }
}