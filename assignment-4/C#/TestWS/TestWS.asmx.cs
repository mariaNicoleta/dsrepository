﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace TestWS
{
    [WebService(Namespace = "http://softwarebydefault.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class TestWebService : System.Web.Services.WebService
    {
        private DatabaseService db = new DatabaseService();

        [WebMethod]
        public void addPackage(PackageEntity packageEntity)
        {
            db.InsertPackage(packageEntity);
        }

        [WebMethod]
        public void removePackage(long id)
        {
            db.DeletePackage(id);
        }

        [WebMethod]
        public void registerForTracking(PackageEntity packageEntity)
        {

        }

        [WebMethod]
        public void addRoute(String city, DateTime time, long packageId)
        {

        }
    }
}