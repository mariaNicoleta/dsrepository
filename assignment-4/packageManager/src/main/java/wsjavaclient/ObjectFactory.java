
package wsjavaclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsjavaclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllRoutesResponse_QNAME = new QName("http://service/", "getAllRoutesResponse");
    private final static QName _GetLogIn_QNAME = new QName("http://service/", "getLogIn");
    private final static QName _GetRegisterResponse_QNAME = new QName("http://service/", "getRegisterResponse");
    private final static QName _GetLogInResponse_QNAME = new QName("http://service/", "getLogInResponse");
    private final static QName _GetRegister_QNAME = new QName("http://service/", "getRegister");
    private final static QName _GetAllRoutes_QNAME = new QName("http://service/", "getAllRoutes");
    private final static QName _GetAllPackages_QNAME = new QName("http://service/", "getAllPackages");
    private final static QName _GetAllPackagesResponse_QNAME = new QName("http://service/", "getAllPackagesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsjavaclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetLogIn }
     * 
     */
    public GetLogIn createGetLogIn() {
        return new GetLogIn();
    }

    /**
     * Create an instance of {@link GetRegisterResponse }
     * 
     */
    public GetRegisterResponse createGetRegisterResponse() {
        return new GetRegisterResponse();
    }

    /**
     * Create an instance of {@link GetAllRoutesResponse }
     * 
     */
    public GetAllRoutesResponse createGetAllRoutesResponse() {
        return new GetAllRoutesResponse();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link GetAllPackages }
     * 
     */
    public GetAllPackages createGetAllPackages() {
        return new GetAllPackages();
    }

    /**
     * Create an instance of {@link GetAllRoutes }
     * 
     */
    public GetAllRoutes createGetAllRoutes() {
        return new GetAllRoutes();
    }

    /**
     * Create an instance of {@link GetLogInResponse }
     * 
     */
    public GetLogInResponse createGetLogInResponse() {
        return new GetLogInResponse();
    }

    /**
     * Create an instance of {@link GetRegister }
     * 
     */
    public GetRegister createGetRegister() {
        return new GetRegister();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link PackageObj }
     * 
     */
    public PackageObj createPackageObj() {
        return new PackageObj();
    }

    /**
     * Create an instance of {@link RouteEntry }
     * 
     */
    public RouteEntry createRouteEntry() {
        return new RouteEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllRoutesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getAllRoutesResponse")
    public JAXBElement<GetAllRoutesResponse> createGetAllRoutesResponse(GetAllRoutesResponse value) {
        return new JAXBElement<GetAllRoutesResponse>(_GetAllRoutesResponse_QNAME, GetAllRoutesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLogIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getLogIn")
    public JAXBElement<GetLogIn> createGetLogIn(GetLogIn value) {
        return new JAXBElement<GetLogIn>(_GetLogIn_QNAME, GetLogIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRegisterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getRegisterResponse")
    public JAXBElement<GetRegisterResponse> createGetRegisterResponse(GetRegisterResponse value) {
        return new JAXBElement<GetRegisterResponse>(_GetRegisterResponse_QNAME, GetRegisterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLogInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getLogInResponse")
    public JAXBElement<GetLogInResponse> createGetLogInResponse(GetLogInResponse value) {
        return new JAXBElement<GetLogInResponse>(_GetLogInResponse_QNAME, GetLogInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRegister }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getRegister")
    public JAXBElement<GetRegister> createGetRegister(GetRegister value) {
        return new JAXBElement<GetRegister>(_GetRegister_QNAME, GetRegister.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllRoutes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getAllRoutes")
    public JAXBElement<GetAllRoutes> createGetAllRoutes(GetAllRoutes value) {
        return new JAXBElement<GetAllRoutes>(_GetAllRoutes_QNAME, GetAllRoutes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getAllPackages")
    public JAXBElement<GetAllPackages> createGetAllPackages(GetAllPackages value) {
        return new JAXBElement<GetAllPackages>(_GetAllPackages_QNAME, GetAllPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getAllPackagesResponse")
    public JAXBElement<GetAllPackagesResponse> createGetAllPackagesResponse(GetAllPackagesResponse value) {
        return new JAXBElement<GetAllPackagesResponse>(_GetAllPackagesResponse_QNAME, GetAllPackagesResponse.class, null, value);
    }

}
