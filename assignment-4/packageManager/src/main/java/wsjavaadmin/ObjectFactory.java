
package wsjavaadmin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsjavaadmin package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddPackage_QNAME = new QName("http://service/", "addPackage");
    private final static QName _AddRoute_QNAME = new QName("http://service/", "addRoute");
    private final static QName _RemovePackage_QNAME = new QName("http://service/", "removePackage");
    private final static QName _AddRouteResponse_QNAME = new QName("http://service/", "addRouteResponse");
    private final static QName _AddPackageResponse_QNAME = new QName("http://service/", "addPackageResponse");
    private final static QName _RegisterForTrackingResponse_QNAME = new QName("http://service/", "registerForTrackingResponse");
    private final static QName _RegisterForTracking_QNAME = new QName("http://service/", "registerForTracking");
    private final static QName _RemovePackageResponse_QNAME = new QName("http://service/", "removePackageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsjavaadmin
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RegisterForTrackingResponse }
     * 
     */
    public RegisterForTrackingResponse createRegisterForTrackingResponse() {
        return new RegisterForTrackingResponse();
    }

    /**
     * Create an instance of {@link AddPackageResponse }
     * 
     */
    public AddPackageResponse createAddPackageResponse() {
        return new AddPackageResponse();
    }

    /**
     * Create an instance of {@link AddRouteResponse }
     * 
     */
    public AddRouteResponse createAddRouteResponse() {
        return new AddRouteResponse();
    }

    /**
     * Create an instance of {@link RemovePackage }
     * 
     */
    public RemovePackage createRemovePackage() {
        return new RemovePackage();
    }

    /**
     * Create an instance of {@link AddRoute }
     * 
     */
    public AddRoute createAddRoute() {
        return new AddRoute();
    }

    /**
     * Create an instance of {@link AddPackage }
     * 
     */
    public AddPackage createAddPackage() {
        return new AddPackage();
    }

    /**
     * Create an instance of {@link RegisterForTracking }
     * 
     */
    public RegisterForTracking createRegisterForTracking() {
        return new RegisterForTracking();
    }

    /**
     * Create an instance of {@link RemovePackageResponse }
     * 
     */
    public RemovePackageResponse createRemovePackageResponse() {
        return new RemovePackageResponse();
    }

    /**
     * Create an instance of {@link PackageObj }
     * 
     */
    public PackageObj createPackageObj() {
        return new PackageObj();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "addPackage")
    public JAXBElement<AddPackage> createAddPackage(AddPackage value) {
        return new JAXBElement<AddPackage>(_AddPackage_QNAME, AddPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "addRoute")
    public JAXBElement<AddRoute> createAddRoute(AddRoute value) {
        return new JAXBElement<AddRoute>(_AddRoute_QNAME, AddRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "removePackage")
    public JAXBElement<RemovePackage> createRemovePackage(RemovePackage value) {
        return new JAXBElement<RemovePackage>(_RemovePackage_QNAME, RemovePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRouteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "addRouteResponse")
    public JAXBElement<AddRouteResponse> createAddRouteResponse(AddRouteResponse value) {
        return new JAXBElement<AddRouteResponse>(_AddRouteResponse_QNAME, AddRouteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "addPackageResponse")
    public JAXBElement<AddPackageResponse> createAddPackageResponse(AddPackageResponse value) {
        return new JAXBElement<AddPackageResponse>(_AddPackageResponse_QNAME, AddPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterForTrackingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "registerForTrackingResponse")
    public JAXBElement<RegisterForTrackingResponse> createRegisterForTrackingResponse(RegisterForTrackingResponse value) {
        return new JAXBElement<RegisterForTrackingResponse>(_RegisterForTrackingResponse_QNAME, RegisterForTrackingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterForTracking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "registerForTracking")
    public JAXBElement<RegisterForTracking> createRegisterForTracking(RegisterForTracking value) {
        return new JAXBElement<RegisterForTracking>(_RegisterForTracking_QNAME, RegisterForTracking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "removePackageResponse")
    public JAXBElement<RemovePackageResponse> createRemovePackageResponse(RemovePackageResponse value) {
        return new JAXBElement<RemovePackageResponse>(_RemovePackageResponse_QNAME, RemovePackageResponse.class, null, value);
    }

}
