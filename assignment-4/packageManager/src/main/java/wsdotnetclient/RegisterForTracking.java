
package wsdotnetclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packageEntity" type="{http://softwarebydefault.com}PackageEntity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packageEntity"
})
@XmlRootElement(name = "registerForTracking")
public class RegisterForTracking {

    protected PackageEntity packageEntity;

    /**
     * Gets the value of the packageEntity property.
     * 
     * @return
     *     possible object is
     *     {@link PackageEntity }
     *     
     */
    public PackageEntity getPackageEntity() {
        return packageEntity;
    }

    /**
     * Sets the value of the packageEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageEntity }
     *     
     */
    public void setPackageEntity(PackageEntity value) {
        this.packageEntity = value;
    }

}
