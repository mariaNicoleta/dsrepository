/*
package com.assignment4.server;

import com.assignment4.entities.App_user;
import com.assignment4.loginservice.ClientService;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

//Service Implementation Bean
@WebService(endpointInterface = "com.assignment4.loginservice.ClientService")
public class ClientServiceImpl implements ClientService {

	@Resource
    WebServiceContext serviceContext;

    @Override
    public App_user getLogIn() {

        MessageContext mctx = serviceContext.getMessageContext();

        //get detail from request headers
        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
        List userList = (List) http_headers.get("Username");
        List passList = (List) http_headers.get("Password");

        String username = "";
        String password = "";

        if(userList!=null){
            username = userList.get(0).toString();
        }

        if(passList!=null){
            password = passList.get(0).toString();
        }

        try {
            return DataBaseHelper.findUser(username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public App_user getRegister() {

        MessageContext mctx = serviceContext.getMessageContext();

        //get detail from request headers
        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);

        List userList = (List) http_headers.get("Username");
        String username = "";
        if(userList!=null){
            username = userList.get(0).toString();
        }

        List passList = (List) http_headers.get("Password");
        String password = "";
        if(passList!=null){
            password = passList.get(0).toString();
        }

        App_user loggingUser = new App_user(username, password, "USER");
        try {
            DataBaseHelper.insertUser(loggingUser);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loggingUser;

    }

}*/
