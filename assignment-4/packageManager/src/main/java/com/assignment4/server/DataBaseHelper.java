/*
package com.assignment4.server;

import com.assignment4.entities.App_user;

import java.sql.*;


*/
/**
 * Created by maria on 12/20/2016.
 *//*

public class DataBaseHelper {

    private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
    private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/assignment-four-db";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    static void insertUser(App_user user) throws SQLException {

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO `app_user` "
                + "(USERNAME, PASSWORD, ROLE) VALUES "
                + "(?,?,?)";

        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole());

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into DBUSER table!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }

    }

    private static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(DB_DRIVER);

        } catch (ClassNotFoundException e) {

            System.out.println(e.getMessage());

        }

        try {

            dbConnection = DriverManager.getConnection(
                    DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return dbConnection;

    }

    private static java.sql.Timestamp getCurrentTimeStamp() {

        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());

    }

    public static App_user findUser(String username, String password) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String findUserSQL = "SELECT * FROM `app_user` "
                + "WHERE USERNAME = ? AND PASSWORD = ?";

        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(findUserSQL);

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            // execute insert SQL stetement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                // return the first one
                return new App_user(username, password, resultSet.getString(2));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return null;
    }
}
*/
