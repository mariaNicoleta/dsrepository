package com.assignment4.ui;

import com.assignment4.entities.RouteEntry;
import com.assignment4.serviceclients.AdminService;
import com.assignment4.serviceclients.ClientService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import wsjavaclient.PackageObj;

import java.util.ArrayList;
import java.util.List;

@DesignRoot
public class AdminView extends HorizontalLayout {

    private ClientService clientService;// = new ClientService();
    private AdminService adminService;// = AdminService.getService("java");

    TextField filter = new TextField();
    Grid packageList = new Grid();
    Button newPackage = new Button("New package");
    PackageForm PackageForm = new PackageForm(this);

    Button newRoute = new Button("New route");
    Grid routesList = new Grid();
    RouteForm routeForm = new RouteForm(this);

    public AdminView() {
        configureComponents();
        buildLayout();
    }

    PackageObj selectedPackage = null;

    private void configureComponents() {
        newPackage.addClickListener(e -> PackageForm.edit(new PackageObj()));

        filter.setInputPrompt("Filter Packages...");
        filter.addTextChangeListener(e -> refreshPackages(e.getText()));

        BeanItemContainer<PackageObj> container = new BeanItemContainer<>(PackageObj.class,
                clientService.getPackages());

        packageList.setContainerDataSource(container);
        packageList.setSelectionMode(Grid.SelectionMode.SINGLE);
        packageList.addSelectionListener(e -> {
            selectedPackage = (PackageObj) packageList.getSelectedRow();
            PackageForm.edit(selectedPackage);
            if (selectedPackage != null && selectedPackage.isTracking()) {
                routesList.setVisible(true);
                newRoute.setEnabled(true);
            } else {
                routesList.setVisible(false);
                newRoute.setEnabled(false);
            }
        });
        refreshPackages();

        newRoute.setEnabled(false);
        newRoute.addClickListener(e -> routeForm.edit(new RouteEntry()));
        List<RouteEntry> routes = new ArrayList<>();
        if (selectedPackage != null) {
            routes = clientService.getRoutes(selectedPackage.getId());
        }
        if (routes == null) {
            routes = new ArrayList<>();
        }
        BeanItemContainer<RouteEntry> routeContainer = new BeanItemContainer<>(RouteEntry.class,
                routes);
        routesList.setContainerDataSource(routeContainer);
        routesList.setSelectionMode(Grid.SelectionMode.SINGLE);
        routesList.addSelectionListener(e -> routeForm.edit((RouteEntry) routesList.getSelectedRow()));
        routesList.setVisible(false);
        refreshRoutes();
    }

    private void buildLayout() {
        HorizontalLayout packageActions = new HorizontalLayout(filter, newPackage);
        packageActions.setSpacing(true);
        packageActions.setExpandRatio(filter, 1);

        VerticalLayout packageLayout = new VerticalLayout(packageActions, packageList);
        packageList.setHeight(300, Unit.PIXELS);
        packageLayout.setSpacing(true);
        packageLayout.setMargin(true);
        packageLayout.setExpandRatio(packageList, 1);

        HorizontalLayout routeActions = new HorizontalLayout(newRoute);
        routeActions.setSpacing(true);

        VerticalLayout routeLayout = new VerticalLayout(routeActions, routesList);
        routesList.setHeight(300, Unit.PIXELS);
        routeLayout.setSpacing(true);
        routeLayout.setMargin(true);
        routeLayout.setExpandRatio(routesList, 1);

        addComponent(new VerticalLayout(packageLayout, routeLayout));
        addComponent(PackageForm);
        addComponent(routeForm);
        setSpacing(true);
        setMargin(true);
    }

    void refreshPackages() {
        refreshPackages(filter.getValue());
    }

    private void refreshPackages(String stringFilter) {
        BeanItemContainer<PackageObj> container = new BeanItemContainer<>(
                PackageObj.class,
                filterPackages(clientService.getPackages(), stringFilter));
        packageList.setContainerDataSource(container);
        PackageForm.setVisible(false);
    }

    private List<PackageObj> filterPackages(List<PackageObj> packages, String stringFilter) {
        List<PackageObj> filtered = new ArrayList<>();
        for (PackageObj packageObj : packages) {
            if (packageObj.getName().contains(stringFilter)) {
                filtered.add(packageObj);
            }
        }
        return filtered;
    }

    public void refreshRoutes() {
        List<RouteEntry> routes = new ArrayList<>();
        if (selectedPackage != null) {
            routes = clientService.getRoutes(selectedPackage.getId());
        }
        if (routes == null) {
            routes = new ArrayList<>();
        }
        BeanItemContainer<RouteEntry> container = new BeanItemContainer<>(RouteEntry.class, routes);
        routesList.setContainerDataSource(container);
        routeForm.setVisible(false);
    }

    private List<RouteEntry> filterRoutes(List<RouteEntry> routes, String filter) {
        List<RouteEntry> filtered = new ArrayList<>();
        for (RouteEntry routeEntry : routes) {
            if (routeEntry.getCity().contains(filter)) {
                filtered.add(routeEntry);
            }
        }
        return filtered;
    }

    public int getSelectedPackageId() {
        return selectedPackage.getId();
    }
}
