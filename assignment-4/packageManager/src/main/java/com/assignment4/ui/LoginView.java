package com.assignment4.ui;

import com.assignment4.serviceclients.ClientService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import wsjavaclient.User;

import java.util.function.Consumer;

@DesignRoot
public class LoginView extends VerticalLayout {

    private ClientService clientService;// = new ClientService();

    TextField username = new TextField("Username:");
    PasswordField password = new PasswordField("Password:");
    Button loginButton = new Button("Log in");
    Button registerButton = new Button("Register");

    public LoginView(Consumer<User> loginCallback) {
        addComponent(new VerticalLayout(username, password, loginButton, registerButton));
        setSizeFull();

        loginButton.addClickListener(click -> {
            final String usernameValue = username.getValue();
            final String passwordValue = password.getValue();
            final User user = clientService.logIn(usernameValue, passwordValue);
            if (user != null) {
                loginCallback.accept(user);
            } else {
                Notification.show("Incorrect username or password!", Notification.Type.ERROR_MESSAGE);
            }
        });

        registerButton.addClickListener(click -> {
            final String usernameValue = username.getValue();
            final String passwordValue = password.getValue();
            try {
                final User user = clientService.register(usernameValue, passwordValue);
                loginCallback.accept(user);
            } catch (Exception e) {
                Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Notification.show("Welcome to package management!");
    }
}
