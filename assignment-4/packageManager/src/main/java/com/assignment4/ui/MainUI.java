package com.assignment4.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
@Theme("valo")
public class MainUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("Package management application");
        addStyleName(ValoTheme.UI_WITH_MENU);
        setResponsive(true);
        setContent(new LoginView(user -> {
            setContent(user.getRole().equalsIgnoreCase("admin")
                    ? new AdminView()
                    : new UserView());
        }));
    }
}