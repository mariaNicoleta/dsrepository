package com.assignment4.entities;

import java.util.Date;

/**
 * Created by maria on 1/5/2017.
 */
public class RouteEntry {

    private String city;
    private Date time;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
