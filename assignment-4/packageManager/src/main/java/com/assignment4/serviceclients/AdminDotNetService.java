package com.assignment4.serviceclients;

import wsdotnetclient.PackageEntity;
import wsdotnetclient.TestWebService;
import wsdotnetclient.TestWebServiceSoap;
import wsjavaclient.PackageObj;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by maria on 1/9/2017.
 */
public class AdminDotNetService extends AdminService {
    TestWebServiceSoap adminService;

    public AdminDotNetService() {
        TestWebService adminPackageService = new TestWebService();
        adminService = adminPackageService.getTestWebServiceSoap();
    }

    public void addPackage(PackageObj packageEntity) {
        packageEntity.setTracking(false);
        adminService.addPackage(toPackageEntity(packageEntity));
    }

    private PackageEntity toPackageEntity(PackageObj packageEntity) {
        PackageEntity packageEntity1 = new PackageEntity();
        packageEntity.setId(packageEntity.getId());
        return packageEntity1;
    }

    public void removePackage(int id) {
        adminService.removePackage(id);
    }

    public void registerForTracking(int packageId) {
        PackageEntity packageEntity = new PackageEntity();
        packageEntity.setId(packageId);
        adminService.registerForTracking(packageEntity);
    }

    public void addRoute(String city, Date date, int packageId) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        try {
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            adminService.addRoute(city, date2, packageId);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }
}
