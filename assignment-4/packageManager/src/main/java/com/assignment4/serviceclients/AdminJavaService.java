package com.assignment4.serviceclients;

import wsdotnetclient.PackageEntity;
import wsjavaadmin.AdminPackageServiceImpl;
import wsjavaadmin.AdminPackageServiceImplService;
import wsjavaadmin.PackageObj;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by maria on 1/9/2017.
 */
public class AdminJavaService extends AdminService {

    AdminPackageServiceImpl adminService;

    public AdminJavaService() {
        AdminPackageServiceImplService clientPackageService = new AdminPackageServiceImplService();
        adminService = clientPackageService.getAdminPackageServiceImplPort();
    }

    @Override
    public void addPackage(wsjavaclient.PackageObj packageEntity) {
        adminService.addPackage(toPackageObj(packageEntity));
    }

    private PackageObj toPackageObj(wsjavaclient.PackageObj packageEntity) {
        PackageObj packageObj = new PackageObj();
        packageEntity.setId(packageEntity.getId());
        packageObj.setSender(packageEntity.getSender());
        packageObj.setReceiver(packageEntity.getReceiver());
        packageObj.setName(packageEntity.getName());
        packageObj.setDestinationCity(packageEntity.getDestinationCity());
        packageObj.setDescription(packageEntity.getDescription());
        packageObj.setSenderCity(packageEntity.getSenderCity());
        packageObj.setTracking(packageEntity.isTracking());
        return packageObj;
    }

    @Override
    public void removePackage(int id) {
        adminService.removePackage(id);
    }

    @Override
    public void registerForTracking(int packageId) {
        adminService.registerForTracking(packageId);
    }

    @Override
    public void addRoute(String city, Date date, int packageId) {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        try {
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            adminService.addRoute(city, date2, packageId);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }
}
