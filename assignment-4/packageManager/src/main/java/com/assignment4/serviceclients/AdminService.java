package com.assignment4.serviceclients;

import wsdotnetclient.PackageEntity;
import wsjavaclient.PackageObj;

import java.util.Date;

/**
 * Created by maria on 1/9/2017.
 */
public abstract class AdminService {

    public static AdminService getService(String serviceType) {
        AdminService service = serviceType.equalsIgnoreCase("java") ?
                new AdminJavaService() : new AdminDotNetService();
        return service;
    }

    abstract public void addPackage(PackageObj packageEntity);

    abstract public void removePackage(int id);

    abstract public void registerForTracking(int packageId);

    abstract public void addRoute(String city, Date date, int packageId);
}
