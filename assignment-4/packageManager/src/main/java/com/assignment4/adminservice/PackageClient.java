/*
package com.assignment4.adminservice;

import com.assignment4.entities.RouteEntry;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


public class PackageClient {

    private static final String PCK_URL = "http://localhost:8888/ws/hello?wsdl";
    private static PackageService packageService = getPackageService();

    public static String useFindAllPackagesService(String stringFilter) {
        Map<String, Object> req_ctx = ((BindingProvider) packageService).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, PCK_URL);

        Map<String, String> headers = new HashMap<>();
        headers.put("Filter", stringFilter);
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        return packageService.findAllPackages();
    }

    public static void useSavePackageService(Package aPackage) {
        Map<String, Object> req_ctx = ((BindingProvider) packageService).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, PCK_URL);

        Map<String, String> headers = new HashMap<>();
        headers.put("Description", aPackage.getDescription());
        headers.put("DestinationCity", aPackage.getDestinationCity());
        headers.put("Name", aPackage.getName());
        headers.put("Receiver", aPackage.getReceiver());
        headers.put("Sender", aPackage.getSender());
        headers.put("City", aPackage.getSenderCity());
        headers.put("Tracking", String.valueOf(aPackage.isTracking()));
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        packageService.savePackage();
    }

    public static void useSaveRouteService(RouteEntry selectedRouteEntry) {
        Map<String, Object> req_ctx = ((BindingProvider) packageService).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, PCK_URL);

        Map<String, RouteEntry> headers = new HashMap<>();
        headers.put("Route", selectedRouteEntry);
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        packageService.saveRoute();
    }

    public static String useFindAllRoutesService(Package selectedPackage) {
        Map<String, Object> req_ctx = ((BindingProvider) packageService).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, PCK_URL);

        Map<String, Package> headers = new HashMap<>();
        headers.put("Package", selectedPackage);
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        return packageService.findAllRoutes();
    }

    private static PackageService getPackageService() {
        URL url;
        try {
            url = new URL(PCK_URL);
        } catch (MalformedURLException e) {
            return null;
        }
        QName qname = new QName("http://packageservice/", "PackageServiceImplService");

        Service service = Service.create(url, qname);
        PackageService packageService = null;
        Iterator<QName> it = service.getPorts();
        while (it.hasNext()) {
            QName port = it.next();
            if ("PackageServiceImplPort".equals(port.getLocalPart())) {
                packageService = service.getPort(port, PackageService.class);
            }
        }
        return packageService;
    }

}
*/
