package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;

@WebService
public class AdminPackageServiceImpl implements AdminPackageService {
    private DBUtils db;

    public AdminPackageServiceImpl() {
        db = new DBUtils();
    }

    @WebMethod
    public void addPackage(PackageObj packageEntity) {
        db.insertPackage(packageEntity);
    }

    @WebMethod
    public void removePackage(int id) {
        db.deletePackage(id);
    }

    @WebMethod
    public void registerForTracking(int packageId) {
        db.registerForTracking(packageId);
    }

    @WebMethod
    public void addRoute(String city, Date time, int packageId) {
        db.addRoute(city, time, packageId);
    }
}
