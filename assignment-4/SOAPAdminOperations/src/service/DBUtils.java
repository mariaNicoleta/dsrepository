package service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by maria on 1/8/2017.
 */
public class DBUtils {

    private static Connection conn = getConnection();

    public static Connection getConnection() {
        if (conn != null)
            return conn;

        InputStream inputStream = DBUtils.class.getClassLoader().getResourceAsStream("db.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            String driver = properties.getProperty("driver");
            String url = properties.getProperty("url");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void closeConnection(Connection toBeClosed) {
        if (toBeClosed == null)
            return;
        try {
            toBeClosed.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void insertPackage(PackageObj packageEntity) {
        try {
            String query = "insert into packages " +
                    "(sender_id, receriver_id, name, description, " +
                    "sender_city, destination_city, tracking) " +
                    "values (?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, packageEntity.getSender());
            preparedStatement.setInt(2, packageEntity.getReceiver());
            preparedStatement.setString(3, packageEntity.getName());
            preparedStatement.setString(4, packageEntity.getDescription());
            preparedStatement.setString(5, packageEntity.getSenderCity());
            preparedStatement.setString(6, packageEntity.getDestinationCity());
            preparedStatement.setBoolean(7, false);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletePackage(int id) {
        try {
            String query = "delete from packages where id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void registerForTracking(int packageId) {
        try {
            String query = "update packages set tracking=? where id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setBoolean(1, true);
            preparedStatement.setInt(2, packageId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addRoute(String city, Date time, int packageId) {
        try {
            String query = "insert into routes " +
                    "(package_id, city, time) " +
                    "values (?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, packageId);
            preparedStatement.setString(2, city);
            preparedStatement.setDate(3, new java.sql.Date(time.getTime()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*public static void main(String[] args) {
        *//*PackageObj pck = new PackageObj();
        pck.setSender(1);
        pck.setReceiver(2);
        pck.setSenderCity("dd");
        pck.setDestinationCity("dd");
        pck.setName("dd");*//*
        DBUtils dbUtils = new DBUtils();
        //dbUtils.insertPackage(pck);
        //dbUtils.registerForTracking(1);
        //dbUtils.deletePackage(1);
        dbUtils.addRoute("fnewio", new Date(573489), 2);
    }*/
}
