package service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by maria on 1/8/2017.
 */
public class DBUtils {

    private static Connection conn = getConnection();

    public static Connection getConnection() {
        if (conn != null)
            return conn;

        InputStream inputStream = DBUtils.class.getClassLoader().getResourceAsStream("db.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            String driver = properties.getProperty("driver");
            String url = properties.getProperty("url");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void closeConnection(Connection toBeClosed) {
        if (toBeClosed == null)
            return;
        try {
            toBeClosed.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User register(String username, String password) {
        try {
            String query = "insert into users " +
                    "(username, password, role) " +
                    "values (?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, "user");
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return logIn(username, password);
    }

    public User logIn(String username, String password) {
        User user = new User();
        try {
            String query = "select * from users where username=? and password=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("user_id"));
                user.setUsername(username);
                user.setPassword(password);
                user.setRole(resultSet.getString("role"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<PackageObj> allPackages() {
        List<PackageObj> packageObjs = new ArrayList<PackageObj>();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from packages");
            while (resultSet.next()) {
                PackageObj packageObj = new PackageObj();
                packageObj.setId(resultSet.getInt("id"));
                packageObj.setSender(resultSet.getInt("sender_id"));
                packageObj.setReceiver(resultSet.getInt("receriver_id"));
                packageObj.setName(resultSet.getString("name"));
                packageObj.setDestinationCity(resultSet.getString("destination_city"));
                packageObj.setDescription(resultSet.getString("description"));
                packageObj.setSenderCity(resultSet.getString("sender_city"));
                packageObj.setTracking(resultSet.getBoolean("tracking"));
                packageObjs.add(packageObj);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return packageObjs;
    }

    /*public static void main(String[] args) {
        DBUtils dbUtils = new DBUtils();
        System.out.println(dbUtils.allPackages());
        System.out.println(dbUtils.register("fewj", "dhi"));
    }*/
}
