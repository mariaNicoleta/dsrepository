package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;


@WebService
public interface ClientPackageService {

    @WebMethod
    User getLogIn(String username, String password);

    @WebMethod
    User getRegister(String username, String password);

    @WebMethod
    List<PackageObj> getAllPackages();
}


